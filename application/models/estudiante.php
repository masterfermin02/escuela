<?php
class Estudiante extends CI_Model {

    
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }
    
    function get_perfil($id){
        $table_name = 'estudiante';
        $this->db->select('*, familia.apellidos as apellido')->from($table_name);
        $this->db->join('familia','familia.idfamilia = estudiante.idfamilia');
        $this->db->where('idestudiante',$id);
        $query = $this->db->get();
        return $query->row_array();
        
    }
    
    function get_cuentas($id){
        //Build contents query
            $this->db->select()->from('cuenta_pendiente');
            $this->db->where('idestudiante',$id);
            $this->db->where('estado','pendiente');
            $this->flexigrid->build_query();
		
            //Get contents
            $return['records'] = $this->db->get();
            
            //Build count query
            $this->db->select('count(1) as record_count')->from('cuenta_pendiente');
            $this->flexigrid->build_query(FALSE);
            $record_count = $this->db->get();
            $row = $record_count->row();
            
            //Get Record Count
            $return['record_count'] = $row->record_count;
        
            //Return all
            return $return;
    }
    
    function change_estatus(){
        $id = $this->input->post('id');
        $this->db->select();
        $this->db->where('idestudiante',$id);
        $query = $this->db->get('estudiante');
        $estado = $query->row_array();
        $nuevo_estado = '';
        if($estado['estatus'] == 'Inactivo' ){
            $nuevo_estado = 'Activo';
            $this->db->set('estatus', $nuevo_estado);
            $this->db->where('idestudiante',$id);
            $this->db->update('estudiante');
        }else{
            $nuevo_estado = 'Inactivo';
            $this->db->set('estatus',$nuevo_estado);
            $this->db->where('idestudiante',$id);
            $this->db->update('estudiante');
        }
        echo $nuevo_estado;
    }
}
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * end file: perfil.php
 * location: application/models
 */