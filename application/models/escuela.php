<?Php

class Escuela extends CI_Model {



    

        function __construct()

        {

            // Call the Model constructor

            parent::__construct();

        }

	

	public function get_table($table = null)

	{

		return $this->db->query("SELECT * FROM ".$table);

	}

	

	public function get_nivel($id)

	{

		$this->db->select();

		$this->db->where('id_nivel', $id);

   		$query = $this->db->get('nivel_cursando');

		return $query->row_array();

	}

        

        public function get_nivel_list()

        {

            //Build contents query

            $table_name = 'nivel_cursando';

            $this->db->select('*, docente.nombre as docente ')->from($table_name);

            $this->db->join('docente','docente.iddocente = nivel_cursando.id_docente');

            $this->flexigrid->build_query();

		

            //Get contents

            $return['records'] = $this->db->get();

            

            //Build count query

            $this->db->select('count(id_nivel) as record_count')->from($table_name);

            $this->flexigrid->build_query(FALSE);

            $record_count = $this->db->get();

            $row = $record_count->row();

            

            //Get Record Count

            $return['record_count'] = $row->record_count;

        

            //Return all

            return $return;

            

        }

	

	public function aumental_estudiante($id_nivel)

	{

		

		$this->db->set('cantidad_estudiante', 'cantidad_estudiante+1',FALSE);

		$this->db->where('id_nivel', $id_nivel);

		$this->db->update('nivel_cursando');

	}

	

	public function get_curso($id)

	{

		$this->db->select();

		$this->db->where('idcurso', $id);

   		$query = $this->db->get('curso');

		return $query->row_array();

	}

	

	

	public function list_cursos_procesando()

	{

		$this->db->select();

		$this->db->where('estado', 'procesando');

   		$query = $this->db->get('curso');

		return $query->result();
	}

	/**

	 *	get_nombre_estudiante

	 *

	 *	@access public

	 * 	@param 	int $id idestudiabte

	 *	@return string

	**/

	public function get_nombre_estudiante($id)

	{

		$this->db->select('nombre');

		$this->db->where('idestudiante', $id);

   		$query = $this->db->get('estudiante');

                if ($query->num_rows() > 0)

                {

                    $estudiante = $query->row_array();

                    return $estudiante['nombre'];

                }

                return "";

		

	}

    

        public function get_estudiante_by_id($id)

        {

            $this->db->select();

                    $this->db->where('idestudiante', $id);

                    $query = $this->db->get('estudiante');

                    return $query->row_array();



        }

	

	/**

	 *	get_estudiantes me devuelve una lista de estudiantes

	 *	con estado activo en fecha_primer_pago < a la fecha actual

	 *

	 *	@access public

	 *	@return array()

	**/

	public function get_estudiantes()

	{

		$query = $this->db->query('

		SELECT * 

		FROM estudiante 

		WHERE estatus = "activo" AND EXTRACT(MONTH FROM fecha_primer_pago) >= EXTRACT(MONTH FROM NOW())');

		return $query->result_array();

	}

	

	/**

	 *	have_cuenta_por_cobra devuelve true si el estudiante tiene alguna

	 *	cuenta por cobrar en este mes.

	 *

	 *	@access public

	 * 	@param 	int $id el id del estudiabte

	 *	@return Boolean

	**/

	public function have_cuenta_por_cobra($id_estudiante)

	{

		$query = $this->db->query('SELECT * FROM cuenta_pendiente WHERE idestudiante ='.$id_estudiante.' AND  EXTRACT(MONTH FROM fecha) = EXTRACT(MONTH FROM NOW()) AND concepto != "Re-Inscripcion"');

   		

		return $query->num_rows();

	}

	

	/**

	 *	have_cuenta_por_cobra devuelve la cuenta_por_cobra si el estudiante tiene alguna

	 *	cuenta por cobrar en mes pasado.

	 *

	 *	@access public

	 * 	@param 	int $id el id del estudiabte

	 *	@return query

	**/

	public function cuenta_pendiente_mes_pasado($id_estudiante)

	{

		$query = $this->db->query(	'SELECT * 

									FROM cuenta_pendiente 

									WHERE idestudiante ='.$id_estudiante.' AND concepto = "Pago Mensual" AND estado = "pendiente" AND recargos !="100" AND EXTRACT(MONTH FROM fecha) < EXTRACT(MONTH FROM NOW())'

								);

   		

		return $query->result();

	}

	

	/**

	 *	set_cuenta_por_cobra agrega una nueva cuenta por cobrar.

	 *

	 *	@access public

	 * 	@param 	int		$id 		el id del estudiabte

	 * 	@param 	decimal	$monto		monto de la cuenta por cobrar

	 * 	@param 	string 	$estado 	estado de la cuenta enum(pagado,pendiente,atrasado)

	 * 	@param 	decimal	$recargo	recargo cuenta por cobrar 

	 * 	@param 	decimal	$total 		total cuenta por cobrar (monto + recargo)

	 * 	@param 	date	$fecha 		now()

	 *	@return void

	**/

	public function set_cuenta_por_cobra($id_estudiante, $concepto, $monto, $estado, $recargos, $total, $fecha)

	{

		$data = array(

			'idestudiante' => $id_estudiante,

                        'concepto' => $concepto,

			'monto' => $monto,

			'estado' => $estado,

			'recargos' => $recargos,

			'total' => $total,

			'fecha' => $fecha

		);

		$this->db->insert('cuenta_pendiente', $data);

	

	}

	

	/**

	 *	get_nivel_monto devuelve el monto mensual del nivel actual del estudiante.

	 *	@access public

	 * 	@param 	int		$id 		el id del estudiabte

	 *	@return decimal

	**/

	public function get_nivel_monto($id_nivel)

	{

		$this->db->select('costo_mensual');

		$this->db->where('id_nivel', $id_nivel);

   		$query = $this->db->get('nivel_cursando');

		$nivel = $query->row_array();

		return $nivel['costo_mensual'];

	}

	

	/**

	 *	set_recargo le agrega el recargo a la cuenta.

	 *	@access public

	 * 	@param 	int		$id 		el id del estudiabte

	 *	@return void

	**/

	public function set_recargo($id_cuenta,$recargo,$total)

	{ 

		$this->db->set('recargos', $recargo);

		$this->db->set('total', $total);

		$this->db->where('idcuenta', $id_cuenta);

   		$this->db->update('cuenta_pendiente');

	}

	

	/**

	 *	get_cuenta_por_cobrar devuelve la cuenta del estudiante

	 *

	 *	@access public

	 * 	@param 	int $id el id del estudiabte

	 *	@return query->result

	**/

	public function get_cuenta_por_cobrar($id_estudiante)

	{

		$query = $this->db->query('SELECT * FROM cuenta_pendiente WHERE idestudiante ='.$id_estudiante.' AND estado = "pendiente"');

   		

		return $query->result();

	}

	

	

	/**

	 *	cuenta_pagar cambia el estado de la cuenta a pagado

	 *

	 *	@access public

	 * 	@param 	int $id el id de la cuenta

	 *	@return void

	**/

	public function cuenta_pagar($id_cuenta)

	{

		$this->db->set('estado', 'Pagado');

		$this->db->where('idcuenta', $id_cuenta);

		$this->db->update('cuenta_pendiente');

	}

	public function devolver_pago($id_cuenta)
	{

		$this->db->set('estado', 'pendiente');

		$this->db->where('idcuenta', $id_cuenta);

		$this->db->update('cuenta_pendiente');
		
	}
	
	public function delete_ingreso($id_cuenta)
	{
		$this->db->where('idcuenta', $id_cuenta);
		$this->db->delete('ingreso',$data);
	}

	public function cantidad_cuentapendiente($id_estudiante)

	{

		$this->db->select();

		$this->db->where('idestudiante', $id_estudiante);

		$this->db->where('estado', 'pendiente');

		   		

		return $this->db->count_all_results('cuenta_pendiente');

	}

	

	

	public function set_ingreso($id_estudiante,$concepto, $monto, $secuencia,$cuenta)

	{

		$data = array(

			'id_estudiante' => $id_estudiante,

			'concepto' => $concepto,

			'fecha' => date("Y-m-d"),

			'monto' => $monto,

                        'id_recibo' => $secuencia,

                        'id_cuenta' => $cuenta

		);

		$this->db->insert('ingreso',$data);

	}

	

	public function set_have_cuenta($id_estudiante,$estado)

	{

		$this->db->set('have_cuenta',$estado);

		$this->db->where('idestudiante', $id_estudiante);

		$this->db->update('estudiante');

	}

	

	public function set_llamado($id_estudiante, $estado)

	{

		$this->db->set('llamado',$estado);

		$this->db->where('idestudiante', $id_estudiante);

		$this->db->update('estudiante');

	}

	

	public function get_cuenta_by_id_cuenta($id_cuenta)

	{

		$this->db->select();

		$this->db->where('idcuenta', $id_cuenta);
		$query = $this->db->get('cuenta_pendiente');

		return $query->row_array();

	}

    

    public function get_estudiente_by_nivel($nivel)

    {

        $this->db->select();

		$this->db->join('familia', 'familia.idfamilia = estudiante.idfamilia');

		//$this->db->join('docente', 'docente.iddocente = nivel_cursando.id_docente');

        $this->db->where('id_nivel_actual', $nivel);

        $this->db->where('estudiante.estatus', 'Activo');

        $query = $this->db->get('estudiante');

		/* $querys = "select e.*, f.apellidos from estudiante e

					join familia f on e.idestudiante = f." */

		//$result = $this->db->query($querys);

        return $query->result();

    }

    

    public function get_nombre_profesor($id_profesor)

    {

        $this->db->select('nombre');

        $this->db->where('iddocente', $id_profesor);

        $query  = $this->db->get('docente');

        return $query->row_array();

    }

    

    public function get_empleados()

    {

        $this->db->select();

        $query = $this->db->get('docente');

        return $query->result();

    }

    

    public function get_turnos($id_empleado)

    {

        $this->db->select();

        $this->db->where('id_docente', $id_empleado);

        

        return $this->db->count_all_results('nivel_cursando');

        

    }

    

    public function get_apellido_familia($id_familia)

    {

        $this->db->select('apellidos');

        $this->db->where('idfamilia', $id_familia);

        $query = $this->db->get('familia');

        $nombre = $query->row_array();

        return $nombre['apellidos'];

    }

    

    public function get_ingreso()

    {

        //Select table name

	$table_name = "ingreso";

	$del = $this->input->post('del');

        $al = $this->input->post('al');

        $sortname = $this->input->post('sortname');

        $sortorder = $this->input->post('sortorder');        

        $rp = $this->input->post('rp');	

        $page = $this->input->post('page');

        $page = $page - 1;

        if($del != "" && $al != "")

        {

            $del = explode("/",$del);

            $al = explode("/",$al);

            $querys = "SELECT ing.id_ingreso AS Ingreso,

                               re.secuencia AS Recibo,

                               es.nombre AS Nombre,

                               fa.apellidos AS Apellido,

                               ing.concepto AS Concepto,

                               case 

                                when ing.id_cuenta = 0 then ''

                                else

                                  DATE_FORMAT(CP.fecha,'%M')

                                end as Mes,

                               ing.fecha AS Fecha,

                               ing.monto AS Monto

                          FROM ingreso ing

                               JOIN estudiante es ON ing.id_estudiante = es.idestudiante

                               JOIN familia fa ON es.idfamilia = fa.idfamilia

                               left JOIN cuenta_pendiente CP ON ing.id_cuenta = CP.idcuenta

                               JOIN recibo re ON  ing.id_recibo = re.secuencia

                          WHERE ing.fecha BETWEEN '$del[2]-$del[0]-$del[1]' AND '$al[2]-$al[0]-$al[1]' 

                          group by ing.id_ingreso

                            order by $sortname $sortorder

                          limit $page,$rp";

            //Get contents

             $query_count = "

                                SELECT ing.id_ingreso

                          FROM ingreso ing

                          WHERE ing.fecha BETWEEN '$del[2]-$del[0]-$del[1]' AND '$al[2]-$al[0]-$al[1]'";

             

            $result = $this->db->query($querys);

            $result_count = $this->db->query($query_count);

            $return['records'] = $result;

            $return['record_count'] = $result_count->num_rows();

            //Return all

            return $return;

        }

        else

        {

            //para hacer la consulta limitado y que el sistema funciones

            //mas rapido

            $querys = "

                        SELECT ing.id_ingreso AS Ingreso,

       re.secuencia AS Recibo,

       es.nombre AS Nombre,

       fa.apellidos AS Apellido,

       ing.concepto AS Concepto,

       case 

        when ing.id_cuenta = 0 then ''

        else

          DATE_FORMAT(CP.fecha,'%M')

        end as Mes,

       ing.fecha AS Fecha,

       ing.monto AS Monto

  FROM ingreso ing

       JOIN estudiante es ON ing.id_estudiante = es.idestudiante

       JOIN familia fa ON es.idfamilia = fa.idfamilia

       left JOIN cuenta_pendiente CP ON ing.id_cuenta = CP.idcuenta

       JOIN recibo re ON  ing.id_recibo = re.secuencia

  group by ing.id_ingreso

                          order by $sortname $sortorder

                          limit $page,$rp";

            

            //para contal el nuemro total de registros

             $query_count = "

                                SELECT ing.id_ingreso,

                                       re.secuencia AS Recibo

                          FROM ingreso ing

                               JOIN estudiante es ON ing.id_estudiante = es.idestudiante

                               JOIN familia fa ON es.idfamilia = fa.idfamilia

                               JOIN recibo re ON  ing.id_recibo = re.secuencia

  group by ing.id_ingreso";

            //Get contents

            $result = $this->db->query($querys);

            $result_count = $this->db->query($query_count);

            $return['records'] = $result;

            $return['record_count'] = $result_count->num_rows();

        

            //Return all

            return $return;

        }

            

        

		

		

        

    }

    

    function get_total_ingreso()

    {

        $del = $this->input->get('del');

        $al = $this->input->get('al');

        $querys = "SELECT ing.id_ingreso AS Ingreso,

                           re.secuencia as Recibo,

                           es.nombre AS Nombre,

                           fa.apellidos AS Apellido,

                           ing.concepto AS Concepto,

                           ing.fecha AS Fecha,

                           ing.monto AS Monto,
						    case
						   when ing.id_cuenta = 0 then ''

                                else

                                  DATE_FORMAT(CP.fecha,'%M')

                                end as Mes

                      FROM ingreso ing

                           JOIN estudiante es ON ing.id_estudiante = es.idestudiante

                           JOIN familia fa ON es.idfamilia = fa.idfamilia

                           join recibo re on re.id_estudiante = ing.id_estudiante
						   left JOIN cuenta_pendiente CP ON ing.id_cuenta = CP.idcuenta
						   ";

        if($del != "" && $al != "")

        {

            $del = explode("/",$del);

            $al = explode("/",$al);

            $querys .= " WHERE ing.fecha BETWEEN '$del[2]-$del[0]-$del[1]' AND '$al[2]-$al[0]-$al[1]'  and ing.fecha = re.fecha";

        }

        

        $querys .= " ";	      



        //Get contents

        $result = $this->db->query($querys);

            

        return $result->result();

    }

    

    function count_grupo_estudiante($id)

    {

        $this->db->select();

        $this->db->from('estudiante');

        $this->db->where('id_nivel_actual',$id);

        $this->db->where('estatus','Activo');

        $query = $this->db->get();

        return $query->num_rows() ;

    }

    

    function agregar_recibo($id_estudiante)

    {

        $this->db->select_max('secuencia');

        $query = $this->db->get('recibo');

        $result = $query->row_array();

        $secuencia = $result['secuencia'] + 1;

        $data = array(

            'secuencia' => $secuencia,

            'id_estudiante' => $id_estudiante,

            'fecha' => date("Y-m-d")

        );

        $this->db->insert('recibo', $data);

        return $secuencia;

    }

    

    function have_cuenta_pendiente($id)

    {

       $query = $this->db->query('SELECT * FROM cuenta_pendiente WHERE idestudiante ='.$id.' AND estado = "pendiente" AND concepto != "Re-Inscripcion" AND EXTRACT(MONTH FROM fecha) < EXTRACT(MONTH FROM NOW())'); 

    

       return ($query->num_rows() > 0);

    }

    

    function general_cuantas_pendientes(){

       $this->db->query(' INSERT INTO cuenta_pendiente(idestudiante,

                             concepto,

                             monto,

                             estado,

                             recargos,

                             total,

                             fecha)

                           SELECT est.idestudiante,

                                  "Pago Mensual",

                                  est.pago_mensual,

                                  "pendiente",

                                  "",

                                  est.pago_mensual,

                                  CURDATE()

                             FROM estudiante est

                            WHERE     est.idestudiante NOT IN (SELECT cp.idestudiante

                                                          FROM cuenta_pendiente cp

                                                               JOIN estudiante est ON est.idestudiante = cp.idestudiante

                                                         WHERE     cp.concepto = "Pago Mensual"

                                                               AND YEAR(cp.fecha) = YEAR(CURDATE())

                                                               AND MONTH(cp.fecha) = MONTH(CURDATE())

                                                               AND est.estatus = "Activo"

                                                        ORDER BY cp.idcuenta ASC

                                                        )

                                       AND est.estatus = "Activo"'

               );

    }

    

    public function set_llamado_all()

    {

            $this->db->set('llamado','No');

            $this->db->update('estudiante');

    }

    
    function tienen_cuentas(){

        $query = $this->db->query('SELECT cp.idestudiante

                                      FROM cuenta_pendiente cp

                                           JOIN estudiante est ON est.idestudiante = cp.idestudiante

                                     WHERE     cp.concepto = "Pago Mensual"

                                           AND YEAR(cp.fecha) = YEAR(CURDATE())

                                           AND MONTH(cp.fecha) = MONTH(CURDATE())

                                           AND est.estatus = "Activo"

                                    ORDER BY cp.idcuenta ASC '

                                    );

        return $query->num_rows(); 

    }

    

    function active_count(){

        $query = $this->db->query('SELECT idestudiante

                          FROM  estudiante 

                         WHERE   estatus = "Activo"

                        ORDER BY idestudiante ASC'

                        );

        return $query->num_rows(); 

    }

    

    function get_cuenta_sin_recargo(){

        $query = $this->db->query('SELECT cp.*,IF(EXTRACT(DAY FROM curdate()) > 15, 1, 0) AS set_recargo

                                  FROM cuenta_pendiente cp

                                 WHERE     cp.concepto = "Pago Mensual"

                                       AND cp.estado = "pendiente"

                                       AND cp.recargos != "100"

                                       AND YEAR(cp.fecha) = YEAR(CURDATE())

                                       AND MONTH(cp.fecha) < MONTH(CURDATE())

                                ORDER BY cp.idcuenta ASC'

                );

        return $query->result();

    }

	

	

}

?>