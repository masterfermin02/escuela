<?php  if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/**
 * reporte CodeIgniter implementation
 *
 * PHP version 5
 *
 * @category  CodeIgniter
 * @package   
 * @author    Fermin Perdomo(masterfermin02@hotmail.com)
 * @version   1.0
 *
*/
    /**
	 * Devuel una string con la cantidad de espacio especificando
	 *
	 * @param	str
	 * @param	size de la cadena 73
	 *
	 * @return	string
	 */
    function fill_space($str = "",$size = 0)
    {
        $cant_space = $size - strlen($str);
        $spaces = " ";
        if($cant_space > 0) 
            $spaces = str_repeat(" ", $cant_space);
        return $str.$spaces;
    }

?>