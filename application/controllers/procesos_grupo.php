<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procesos_grupo extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');	
		$this->load->model('escuela');
		$this->load->helper('form');
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
	}
    
    function _proceso_output($output = null)
    {
	$this->load->view('header.php',$output);
        $this->load->view('menu');
	$this->load->view('proceso.php',$output);	
	$this->load->view('footer.php');		
    }

    function index()
    {
        $crud = new grocery_CRUD();

        $crud->set_theme('datatables');
        $crud->set_table('nivel_cursando');
        $crud->set_subject('Nivel');
        $crud->set_table_title('Nivel Cursando');
        $crud->set_relation('id_docente','docente','nombre');
        $crud->set_relation_n_n('Dias', 'dias_nivel', 'dias_laborables', 'idnivel', 'iddia', 'nombre');
        $crud->unset_fields('cantidad_estudiante','costo_mensual');
        $crud->unset_columns('costo_mensual');
        $crud->display_as('id_docente','Profesor')
        ->display_as('unidades_actuales','Unidades Actuales')
        ->display_as('cantidad_estudiante','Cantidad Estudiante');
        $crud->add_action('Ver Grupo', base_url('imagenes/add.png'), 'procesos_grupo/show_estudiantes','ui-icon-plus');
        $crud->callback_column('cantidad_estudiante',array($this,'_get_cantidad_estudiante'));
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $output = $crud->render();

        $this->_proceso_output($output);
    }
    
    function _get_cantidad_estudiante($value,$row)
    {
        return $this->escuela->count_grupo_estudiante($row->id_nivel);
    }

    function show_estudiantes($id_nivel)
    {
        $crud = new grocery_CRUD();
		
		$crud->set_table('nivel_cursando');
        $crud->set_subject('Nivel');
        $crud->set_table_title('Niveles');
        $crud->set_relation('id_docente','docente','nombre');
        $crud->set_relation_n_n('Dias', 'dias_nivel', 'dias_laborables', 'idnivel', 'iddia', 'nombre');
        $crud->unset_fields('cantidad_estudiante','costo_mensual');
        $crud->unset_columns('costo_mensual');
        $crud->display_as('id_docente','Profesor')
        ->display_as('unidades_actuales','Unidades Actuales')
        ->display_as('cantidad_estudiante','Cantidad Estudiante');
        $crud->where('id_nivel', $id_nivel);
	$crud->unset_add();
        $crud->unset_delete();
        //se usaba ante para generar la re-incriscion automatica
        //$crud->callback_before_update(array($this,'_general_cuenta'));
        $crud->add_action('Re-inscripcion', base_url('assets/grocery_crud/themes/flexigrid/css/images/add.png'),
                            '', "genera_re-inscripcion",array($this,"_action_callback"));
	$output = $crud->render();

	$this->load->view('header.php',$output);
	$data['estudiantes'] = $this->escuela->get_estudiente_by_nivel($id_nivel);
	$data['grupo_id'] = $id_nivel;
        $this->load->view('header.php',$output);
        $this->load->view('menu');
       	$this->load->view('procesos_grupo.php',$data,$output);	
	$this->load->view('footer.php');	
    }
    
    function _action_callback($key,$row)
    {
        return $key;
    }


    function general_cuenta($primary_key)
    {
       $estudiantes = $this->escuela->get_estudiente_by_nivel($primary_key);
        foreach ($estudiantes as $estudiante)
        {
            if($estudiante->monto_reinscripcion > 0)
            {
                $this->escuela->set_cuenta_por_cobra($estudiante->idestudiante, 'Re-Inscripcion', 
                                                     $estudiante->monto_reinscripcion, 'pendiente', 0, $estudiante->monto_reinscripcion, date("Y-m-d"));
                $this->escuela->set_llamado($estudiante->idestudiante, 'No');
		$this->escuela->set_have_cuenta($estudiante->idestudiante, 'Si');
            }
        }
    }
    
    
}
?>