<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Procesos extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->helper('url');
		$this->load->library('grocery_CRUD');	
		$this->load->model('escuela');
		$this->load->helper('form');
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
	}
	
	
	function _proceso_output($output = null)
	{
		$this->load->view('header.php',$output);
                $this->load->view('menu');
		$this->load->view('proceso.php',$output);	
		$this->load->view('footer.php');		
	}
		
	function index()
	{
		$this->load->view('autocomplete.php');
	}
	
	function cuentas_pendientes($id)
	{
           
		$crud = new grocery_CRUD();
			
		$array = array('idestudiante' => $id, 'estado' => "pendiente");
                
		$crud->where($array);
		$crud->set_table_title('Cuentas Pendientes');
		$crud->set_table('cuenta_pendiente');
		$crud->set_subject('cuenta pendiente');
		$crud->fields('idestudiante', 'concepto','fecha','monto','recargos', 'total','estado' );
		$crud->field_type('idestudiante','invisible');
		$crud->field_type('concepto','invisible');
		$crud->field_type('total','invisible');
		$crud->field_type('estado','invisible');
		$crud->display_as('idestudiante','Nombre Estudiante');
		$crud->callback_column('idestudiante',array($this,'callback_column_estudiante'));
		$crud->callback_before_insert(array($this,'callback_before_insert_cuenta_pendiente'));
		
		
		$output = $crud->render();
		
		$this->_proceso_output($output);
	}
	
	function callback_column_estudiante($value,$row)
	{
		return $this->escuela->get_nombre_estudiante($value);
	}
	
	function callback_before_insert_cuenta_pendiente($post_array)
	{
		$post_array['estado'] = "pendiente";
		//Consigor el id del estudiante en la uri
		$id = $this->uri->segment(3);
		$post_array["idestudiante"] = $id;
		$post_array["concepto"] = "Pago Mensual";
		$post_array["total"] = $post_array["monto"] + $post_array["recargos"];
		$this->escuela->set_llamado($id, 'No');
		$this->escuela->set_have_cuenta($id, 'Si');
		return $post_array;
	}
	
	function general_cuenta_pendiente()
	{
		if($this->escuela->tienen_cuentas() < $this->escuela->active_count()){
                    $this->escuela->general_cuantas_pendientes();
                    $this->escuela->set_llamado_all();
                }
                $this->_general_recargo();
		redirect('pago', 'refresh');
	
	}
	
	function _general_recargo()
	{
           
            $resultado = $this->escuela->get_cuenta_sin_recargo();
            foreach($resultado as $row)
            {
                    if($row->set_recargo == 1)
                     $this->escuela->set_recargo($row->idcuenta, '100', $row->monto + 100);
            }

	}
	
	function nomina()
	{
        if (!$this->ion_auth->in_group('admin'))
		{
			$this->session->set_flashdata('message', 'Tiene que ser admin para ver esta pagina');
			redirect('/');
		}
		$empleados = $this->escuela->get_empleados();
		$nomina = array();
		foreach($empleados as $empleado)
		{
			$turnos = $this->escuela->get_turnos($empleado->iddocente);
			if($turnos == 0)
			{
				$turnos = 1;
			}
			$item = array(
				'nombre' => $empleado->nombre,
				'familia' => $this->escuela->get_apellido_familia($empleado->id_familia),
				'turnos' => $turnos,
				'monto'	=> $empleado->monto_a_cobrar,
				'total' => $turnos * $empleado->monto_a_cobrar
				
			);
			$nomina[] = $item;
		}
		//print_r($nomina);
		$data['nominas'] = $nomina;
        $output = (object)array('output' => '' , 'js_files' => array() , 'css_files' => array());
		$this->load->view('header.php',$output);
        $this->load->view('menu');
		$this->load->view('nomina.php',$data);
	}
				
	function suggestions()
	{
		$this->load->model('autocomplete_model');
		$term = 'p';//$this->input->post('term',TRUE);

		//if (strlen($term) < 2) break;
		$rows = $this->autocomplete_model->GetAutocomplete(array('keyword' => $term));

		
		$sugerencia = array();
		foreach ($rows as $row)
			 array_push($sugerencia, $row->nombre);
		
		$json_array = array(
			'query' => 'p',
			'suggestions' => $sugerencia,
			data => array('LR', 'LY', 'LI', 'LT')
		);

		return json_encode($json_array);
	}

}
	
?>