<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Email extends CI_Controller {


	function __construct()
    {
        parent::__construct();
        $this->load->database();
		$this->load->helper('url');
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
    }

    function index()
    {
    	$output = (object)array('output' => '' , 'js_files' => array() , 'css_files' => array());

    	$this->load->view('header.php',$output);
        $this->load->view('menu');
        $this->load->view('email/email.php',$output);	
        $this->load->view('footer.php');	
    }

}