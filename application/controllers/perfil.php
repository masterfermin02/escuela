<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends CI_Controller {
	
	function __construct()
	{
            parent::__construct();
            $this->load->library('ion_auth');
            $this->load->helper('flexigrid');
            $this->load->library('flexigrid');
            $this->load->database();
            $this->load->helper('url');
            if (!$this->ion_auth->logged_in())
            {
                    redirect('auth/login');
            }
            
            $this->load->model('estudiante');
        }
        
        function index(){
            
        }
         
        function mostrar($id){
            $est = $this->estudiante->get_perfil($id);
            $datos['estu'] = $est;
            $datos['cuentas'] = $this->_load_cuentas($id); 
            $datos['grupo'] = $this->grupo($id);
            $output = (object)array('output' => '' , 'js_files' => array() , 'css_files' => array());
            $this->load->view('menu');
            $this->load->view('perfil',$datos);
        }
        
        function _load_cuentas($id){
            /*
             * 0 - display name
             * 1 - width
             * 2 - sortable
             * 3 - align
             * 4 - searchable (2 -> yes and default, 1 -> yes, 0 -> no.)
             */
            $colModel['concepto'] = array('Concepto',200,TRUE,'center',2);
            $colModel['estado'] = array('Estado',204,TRUE,'center',2);
            $colModel['monto'] = array('Monto',50,TRUE,'left',2);
            $colModel['recargos'] = array('Recargos',100,TRUE,'left',2);
            $colModel['Total'] = array('total',50,TRUE,'left',2);
            $colModel['fecha'] = array('Fecha',80,TRUE,'left',2);
            //$colModel['cantidad_estudiante'] = array('Cantidad estudiante',40,TRUE,'left',2);

            /*
             * Aditional Parameters
             */
            $gridParams = array(
            'width' => '100%',
            'height' => 200,
            'rp' => 5,
            'rpOptions' => '[5,10,15,20,25,40,100,1000,2000,100000]',
            'pagestat' => 'Displaying: {from} to {to} of {total} items.',
            'blockOpacity' => 0.5,
            'title' => '',
            'usepager'=> true,
            'useRp' => true,
            'showTableToggleBtn' => true,

            );

            /*
             * 0 - display name
             * 1 - bclass
             * 2 - onpress
             */
            //$buttons[] = array('Delete','delete','test');
            $buttons[] = array('separator');

            //Build js
            //View helpers/flexigrid_helper.php for more information about the params on this function
            $grid_js = build_grid_js('flex1',site_url("perfil/search_cuentas/".$id),$colModel,'id','asc',$gridParams,$buttons);
            return $grid_js;
        }
        
        function grupo($id){
            /*
             * 0 - display name
             * 1 - width
             * 2 - sortable
             * 3 - align
             * 4 - searchable (2 -> yes and default, 1 -> yes, 0 -> no.)
             */
            $colModel['concepto'] = array('Concepto',200,TRUE,'center',2);
            $colModel['estado'] = array('Estado',204,TRUE,'center',2);
            $colModel['monto'] = array('Monto',50,TRUE,'left',2);
            $colModel['recargos'] = array('Recargos',150,TRUE,'left',2);
            $colModel['Total'] = array('total',50,TRUE,'left',2);
            $colModel['fecha'] = array('Fecha',80,TRUE,'left',2);
            //$colModel['cantidad_estudiante'] = array('Cantidad estudiante',40,TRUE,'left',2);

            /*
             * Aditional Parameters
             */
            $gridParams = array(
            'width' => '100%',
            'height' => 200,
            'rp' => 5,
            'rpOptions' => '[5,10,15,20,25,40,100,1000,2000,100000]',
            'pagestat' => 'Displaying: {from} to {to} of {total} items.',
            'blockOpacity' => 0.5,
            'title' => '',
            'usepager'=> true,
            'useRp' => true,
            'showTableToggleBtn' => true,

            );

            /*
             * 0 - display name
             * 1 - bclass
             * 2 - onpress
             */
            //$buttons[] = array('Delete','delete','test');
            $buttons[] = array('separator');

            //Build js
            //View helpers/flexigrid_helper.php for more information about the params on this function
            $grid_js = build_grid_js('flex1',site_url("perfil/search_grupos".$id),$colModel,'id','asc',$gridParams,$buttons);
            return $grid_js;
        }
        
        function search_cuentas($id){
             $valid_fields = array('concepto','estado','fecha');

            $this->flexigrid->validate_post('fecha','asc',$valid_fields);

            $records = $this->estudiante->get_cuentas($id);

            $this->output->set_header($this->config->item('json_header'));

            /*
             * Json build WITH json_encode. If you do not have this function please read
             * http://flexigrid.eyeviewdesign.com/index.php/flexigrid/example#s3 to know how to use the alternative
             */
            $record_items = array();
            $monto_total = 0;
            foreach ($records['records']->result() as $row)
            {
                    $record_items[] = array($row->idcuenta,
                        $row->concepto,
                        $row->estado,
                        $row->monto,
                        $row->recargos,
                        $row->total,
                        $row->fecha
                            );
           }
                    //Print please
            $this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items,$monto_total));
        }
        
        function change_estatus()
        {
            $this->estudiante->change_estatus();
        }
        
        function search_grupos()
        {
            
        }
        
}
/**
 * end file: perfil.php
 * location: application/controllers
 */