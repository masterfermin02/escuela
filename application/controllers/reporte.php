<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reporte extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('ion_auth');
		
		$this->load->database();
		$this->load->helper('url');
                $this->load->helper('reporte_helper');
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		$this->load->model('escuela');
		
	}
    
    function inscripcion($id)
    {
        $estudiante = $this->escuela->get_estudiante_by_id($id);
        $nivel = $this->escuela->get_nivel($estudiante['id_nivel_actual']);
        $data['familia'] = $this->escuela->get_apellido_familia($estudiante['idfamilia']);
		$secuencia = $this->escuela->agregar_recibo($id);
        $data['secuencia'] = $secuencia;
        $data['direccion'] = $estudiante['direccion'];
        $data['telefono'] = $estudiante['telefono'];
        $monto_total = $estudiante['monto_inscripcion'];
        //Second table: specify 3 columns
        $data['results'][] = $estudiante['idestudiante'];
        $data['results'][] = $estudiante['nombre'];
        $data['results'][] = $nivel['unidades_actuales'];
        $data['results'][] = $monto_total;
        $data['monto_total'] = $monto_total;
        $this->load->view('reporte/recibo_inscripcion',$data);
        /*
        $this->load->library('pdf');
        define('FPDF_FONTPATH','application/libraries/font/'); //si lo defines en el config, podria ser asi: BASEPATH."application/libraries/font/"
        $pdf=new PDF();
        $estudiante = $this->escuela->get_estudiante_by_id($id);
        $nivel = $this->escuela->get_nivel($estudiante['id_nivel_actual']);
        
        
        $pdf->AddPage();
        //Datos de la familia y el estudiante
        $pdf->SetFont('arial','B',10);
        $pdf->cell(0,5,'Familia......: '.$this->escuela->get_apellido_familia($estudiante['idfamilia']),0,1,'L');
        $pdf->cell(0,5,utf8_decode('Dirección...: ').$estudiante['direccion'],0,1,'L');
        $pdf->cell(0,5,utf8_decode('Teléfono....: ').$estudiante['telefono'],0,1,'L');
        $pdf->ln(4);
         $telefono = $estudiante['telefono'];
        $concepto = 'Pago Inscripción';
        $monto_total = $estudiante['monto_inscripcion'];
        //Second table: specify 3 columns
        $pdf->AddCol('idestudiante',30,'Matricula','C');
        $pdf->AddCol('nombre',40,'Nombre');
        $pdf->AddCol('libro',40,'Libro','R');
        $pdf->AddCol('unidades_actuales',40,'Unidad Actual','R');
        $pdf->AddCol('monto_inscripcion',40,'Monto Inscripcion','R');
        $prop=array('HeaderColor'=>array(255,150,100),
                    'color1'=>array(210,245,255),
                    'color2'=>array(255,255,210),
                    'padding'=>2);
        $pdf->Table('select estudiante.idestudiante, estudiante.nombre, estudiante.monto_inscripcion, nivel_cursando.libro, nivel_cursando.unidades_actuales 
            from estudiante join nivel_cursando on nivel_cursando.id_nivel = estudiante.id_nivel_actual 
            where estudiante.idestudiante = '.$id);
        $pdf->ln();
        $pdf->cell(150);
        $pdf->cell(40,5,'Total : $ '.number_format($estudiante['monto_inscripcion']),1,1,'R');
        $pdf->cell(0,5,'____________________',0,1,'L');
        $pdf->cell(0,5,'Recibido Por',0,1,'L');
        $pdf->Output();*/
    }
    
     
	function export_ingreso()
	{
            $data['ingresos'] = $this->escuela->get_total_ingreso();
            $this->load->view('exportar/ingreso',$data);
	}
	
	function export_group($id_nivel)
	{
		
			$data['estudiantes'] = $this->escuela->get_estudiente_by_nivel($id_nivel);
		$this->load->view('exportar/grupo',$data);
	}
    
    function ingreso()
    {
       
        $data['ingresos'] = $this->escuela->get_total_ingreso();
	$this->load->view('reporte/ingreso',$data);
    }
    
    function export_nomina()
    {
        if (!$this->ion_auth->in_group('admin'))
		{
			$this->session->set_flashdata('message', 'Tiene que ser admin para ver esta pagina');
			redirect('/');
		}
		$empleados = $this->escuela->get_empleados();
		$nomina = array();
		foreach($empleados as $empleado)
		{
			$turnos = $this->escuela->get_turnos($empleado->iddocente);
			if($turnos == 0)
			{
				$turnos = 1;
			}
			$item = array(
				'nombre' => $empleado->nombre,
				'familia' => $this->escuela->get_apellido_familia($empleado->id_familia),
				'turnos' => $turnos,
				'monto'	=> $empleado->monto_a_cobrar,
				'total' => $turnos * $empleado->monto_a_cobrar
				
			);
			$nomina[] = $item;
		}
		//print_r($nomina);
		$data['nominas'] = $nomina;
        $this->load->view('exportar/nomina.php',$data);
    }
    
    function prueva()
    {
        $this->load->library('pdf');
        define('FPDF_FONTPATH','application/libraries/font/'); //si lo defines en el config, podria ser asi: BASEPATH."application/libraries/font/"
        $pdf=new PDF();
        $id = 29;
        $estudiante = $this->escuela->get_estudiante_by_id($id);
        $nivel = $this->escuela->get_nivel($estudiante['id_nivel_actual']);
        
        
        $pdf->AddPage();
        //Datos de la familia y el estudiante
        $pdf->SetFont('Arial','',10);
        $pdf->cell(0,5,'Familia......: '.$this->escuela->get_apellido_familia($estudiante['idfamilia']),0,1,'L');
        $pdf->cell(0,5,'Direccion...: '.$estudiante['direccion'],0,1,'L');
        $pdf->cell(0,5,'Telefono....: '.$estudiante['telefono'],0,1,'L');
        $pdf->ln(4);
        $telefono = $estudiante['telefono'];
        $concepto = 'Pago Inscripción';
        $monto_total = $estudiante['monto_inscripcion'];
        //Second table: specify 3 columns
        $pdf->AddCol('idestudiante',30,'Matricula','C');
        $pdf->AddCol('nombre',40,'Nombre');
        $pdf->AddCol('libro',40,'Libro','R');
        $pdf->AddCol('unidades_actuales',40,'Unidad Actual','R');
        $pdf->AddCol('monto_inscripcion',40,'Monto Inscripcion','R');
        $prop=array('HeaderColor'=>array(255,150,100),
                    'color1'=>array(210,245,255),
                    'color2'=>array(255,255,210),
                    'padding'=>2);
        $pdf->Table('select estudiante.idestudiante, estudiante.nombre, estudiante.monto_inscripcion, nivel_cursando.libro, nivel_cursando.unidades_actuales 
            from estudiante join nivel_cursando on nivel_cursando.id_nivel = estudiante.id_nivel_actual 
            where estudiante.idestudiante = '.$id,$prop);
        $pdf->ln();
        $pdf->cell(150);
        $pdf->cell(40,5,'Total : $ '.number_format($estudiante['monto_inscripcion']),1,1,'R');
        $pdf->Output();
    }
   
}

    ?>