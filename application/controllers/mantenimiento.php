<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mantenimiento extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('ion_auth');
		$this->load->helper('flexigrid');
                $this->load->library('flexigrid');
		$this->load->database();
		$this->load->helper('url');
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		$this->load->model('escuela');
		$this->load->library('grocery_CRUD');
		
	}
	
	function _mantenimiento_output($output = null)
	{
		$this->load->view('header.php',$output);
                $this->load->view('menu.php');
		$this->load->view('mantenimiento.php',$output);	
		$this->load->view('footer.php');		
	}
	
	function index()
	{
		$output = (object)array('output' => '' , 'js_files' => array() , 'css_files' => array());
		$this->load->view('header.php',$output);
                $this->load->view('menu');
		$this->load->view('index.php');
		$this->load->view('footer.php');
	}
	
	function nivel_cursando()
	{
      
		$crud = new grocery_CRUD();
		
		$crud->set_table('nivel_cursando');
                $crud->set_theme('datatables');
		$crud->set_subject('Nivel');
		$crud->set_table_title('Nivel Cursando');
		$crud->set_relation('id_docente','docente','nombre');
		$crud->set_relation_n_n('Dias', 'dias_nivel', 'dias_laborables', 'idnivel', 'iddia', 'nombre');
		$crud->unset_fields('cantidad_estudiante','costo_mensual');
		$crud->unset_columns('costo_mensual');
		$crud->display_as('id_docente','Profesor')
		->display_as('unidades_actuales','Unidades Actuales')
		->display_as('cantidad_estudiante','Cantidad Estudiante');
		
		$output = $crud->render();

		$this->_mantenimiento_output($output);
		
	}
	
	function familia()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_table('familia');
			$crud->set_subject('familia');
                        $crud->set_theme('datatables');
			$crud->set_table_title('Familias');
			$crud->required_fields('apellidos');
			$crud->required_fields('nombre');
			$crud->required_fields('correo');
			$crud->required_fields('fechaingreso');
			$crud->columns('nombre','apellidos','correo','fechaingreso');
			$crud->display_as('fechaingreso', 'Fecha Ingreso');
           	$output = $crud->render();
		
			$this->_mantenimiento_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function padre()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_table('padre');
			$crud->set_subject('Familiar');
			$crud->set_table_title('Familiares');
			$crud->set_relation('idfamilia','familia','apellidos');
			$crud->display_as('idfamilia', 'Apellidos Familia')
				->display_as('telefono_empresa', 'Telefono Empresa');
			$crud->required_fields('idfamilia','nombre');
			$crud->fields('idfamilia','nombre','telefono','miembro','Trabaja','empresa','telefono_empresa');
			$output = $crud->render();
			
			$this->_mantenimiento_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function instrumento()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_table('idiomas');
			$crud->set_subject('Idioma');
			$crud->set_table_title('Idiomas');
			$crud->required_fields('nombre');
			$crud->columns('nombre');
			$output = $crud->render();
			
			$this->_mantenimiento_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function estudiante($id = 0)
	{
            try{
			$crud = new grocery_CRUD();

			$crud->set_table('estudiante');
                        $crud->set_theme('datatables');
			$crud->set_subject('estudiante');
			$crud->set_table_title('Estudiantes Activos');
                        $crud->where('estatus','activo');
			$crud->set_relation('idfamilia','familia','apellidos');
			$crud->set_relation('id_nivel_actual','nivel_cursando','{libro} Unidades: {unidades_actuales} Horario:{horario}');
			$crud->display_as('idfamilia', 'Apellidos Familia')
				 ->display_as('estadocivil', 'Estado Civil')
				 ->display_as('fechaingreso', 'Fecha Ingreso')
				 ->display_as('fechanacimiento', 'Fecha Nacimiento')
				 ->display_as('id_nivel_actual', 'Nivel Actual')
				 ->display_as('direccion','Dirección')
				 ->display_as('telefono', 'Teléfono Residencial')
				 ->display_as('monto_inscripcion', 'Monto Inscripción')
				 ->display_as('condicion', 'Condición')
				 ->display_as('pago_mensual', 'Pago Mensual')
				 ->display_as('nombre_pariente', 'Nombre Pariente')
				   ->display_as('telefono_pariente', 'Teléfono Pariente')
				   ->display_as('monto_reinscripcion', 'Monto Reinscripción')
				 ->display_as('fecha_primer_pago', 'Fecha Primer Pago:');
			$crud->columns('nombre','idfamilia','id_nivel_actual', 'profesor','estatus', 'direccion', 'telefono', 
			'celular', 'nacionalidad','fechanacimiento','edad','enfermedades','estadocivil','sexo','fechaingreso',
			'observaciones','monto_inscripcion','pago_mensual', 'nombre_pariente', 'telefono_pariente','monto_reinscripcion');	 
							 
			$crud->required_fields('idfamilia','nombre','monto_inscripcion','pago_mensual', 'monto_reinscripcion');
			$crud->fields('id_nivel_actual','idfamilia', 'condicion','estatus', 'nombre', 'direccion', 'telefono', 
			'celular', 'nacionalidad','fechanacimiento','edad','enfermedades','estadocivil','sexo','fechaingreso',
			'observaciones','monto_inscripcion','pago_mensual','fecha_primer_pago', 'nombre_pariente', 'telefono_pariente','monto_reinscripcion');
			
			$crud->callback_column('profesor',array($this,'_get_nombre_profesor'));
                        $crud->callback_column('nombre',array($this,'_set_nombre_url'));
                       	$crud->field_type('condicion', 'hidden', 'Nuevo');
                        $crud->field_type('fecha_primer_pago', 'invisible');
                        $crud->field_type('id_nivel_actual', 'hidden');
			$crud->add_action('Agregar Cuenta', base_url('imagenes/add.png'), 'procesos/cuentas_pendientes','ui-icon-plus');	
			$crud->add_action('Print Recibo', base_url('imagenes/print.png'), 'reporte/inscripcion', 'ui-icon-plus');
                        if (!$this->ion_auth->in_group('admin'))
                        {
                            $crud->unset_edit();
                            $crud->unset_delete();
                        }
                        $crud->callback_before_insert(array($this,'aumental_cantidad_estudiante'));
                        $crud->callback_after_insert(array($this,'estudiante_after_insert'));
                         $id = $this->uri->segment(4);
                        $msj ='Los datos se guardaron correctamente <a href='.site_url('reporte/inscripcion/'.$id).'>Imprimir Nuevo Ingreso</a> <div style="display:none">';
                        $crud->set_lang_string('insert_success_message', $msj);
                        $output = $crud->render();
			$state = $crud->getState();
                        if(($state == 'add' || $state == 'edit') && $crud->get_table() == 'estudiante')
                        {
                            //ver lib
		
                            /*
                             * 0 - display name
                             * 1 - width
                             * 2 - sortable
                             * 3 - align
                             * 4 - searchable (2 -> yes and default, 1 -> yes, 0 -> no.)
                             */
                            $colModel['id_nivel'] = array('Id nivel',40,TRUE,'center',2);
                            $colModel['libro'] = array('Libro',204,TRUE,'center',2);
                            $colModel['unidades'] = array('Unidades',270,TRUE,'left',2);
                            $colModel['unidads_actuales'] = array('Unidades Actuales',180,TRUE,'left',2);
                            $colModel['docente'] = array('Docente',100,TRUE,'left',2);
                            $colModel['horario'] = array('Horario',417,TRUE,'left',2);
                            //$colModel['cantidad_estudiante'] = array('Cantidad estudiante',40,TRUE,'left',2);
                            
                            /*
                             * Aditional Parameters
                             */
                            $gridParams = array(
                            'width' => '100%',
                            'height' => 200,
                            'rp' => 5,
                            'rpOptions' => '[5,10,15,20,25,40,100,1000,2000,100000]',
                            'pagestat' => 'Displaying: {from} to {to} of {total} items.',
                            'blockOpacity' => 0.5,
                            'title' => 'Niveles',
                            'usepager'=> true,
                            'useRp' => true,
                            'showTableToggleBtn' => true,

                            );
		
                            /*
                             * 0 - display name
                             * 1 - bclass
                             * 2 - onpress
                             */
                            //$buttons[] = array('Delete','delete','test');
                            $buttons[] = array('separator');

                            //Build js
                            //View helpers/flexigrid_helper.php for more information about the params on this function
                            $grid_js = build_grid_js('flex2',site_url("consultas/nivel_cursando"),$colModel,'id','asc',$gridParams,$buttons);

                            $data['js_grid'] = $grid_js;
                            $data['version'] = "0.36";
                            $data['download_file'] = "Flexigrid_CI_v0.36.rar";
                            $output->list_curso = $grid_js; 
                        }
			$this->_mantenimiento_output($output);
		
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
                 
	}
        
        function estudiante_inactivos()
	{
            try{
			$crud = new grocery_CRUD();

			$crud->set_table('estudiante');
                        $crud->set_theme('datatables');
			$crud->set_subject('estudiante');
			$crud->set_table_title('Estudiantes Inactivos');
			$crud->where('estatus','Inactivo');
			$crud->or_where('estatus','Retirado');
			$crud->set_relation('idfamilia','familia','apellidos');
			$crud->set_relation('id_nivel_actual','nivel_cursando','{libro} Unidades: {unidades_actuales} Horario:{horario}');
			$crud->display_as('idfamilia', 'Apellidos Familia')
				 ->display_as('estadocivil', 'Estado Civil')
				 ->display_as('fechaingreso', 'Fecha Ingreso')
				 ->display_as('fechanacimiento', 'Fecha Nacimiento')
				 ->display_as('id_nivel_actual', 'Nivel Actual')
				 ->display_as('direccion','Dirección')
				 ->display_as('telefono', 'Teléfono Residencial')
				 ->display_as('monto_inscripcion', 'Monto Inscripción')
				 ->display_as('condicion', 'Condición')
				 ->display_as('pago_mensual', 'Pago Mensual')
				 ->display_as('nombre_pariente', 'Nombre Pariente')
				   ->display_as('telefono_pariente', 'Teléfono Pariente')
				   ->display_as('monto_reinscripcion', 'Monto Reinscripción')
				 ->display_as('fecha_primer_pago', 'Fecha Primer Pago:');
			$crud->unset_add();
			$crud->unset_edit();
                        $crud->callback_column('nombre',array($this,'_set_nombre_url'));
			$crud->columns('nombre','idfamilia','id_nivel_actual', 'profesor','estatus', 'direccion', 'telefono', 
			'celular', 'nacionalidad','fechanacimiento','edad','enfermedades','estadocivil','sexo','fechaingreso',
			'observaciones','monto_inscripcion','pago_mensual', 'nombre_pariente', 'telefono_pariente','monto_reinscripcion');
			$output = $crud->render();
			$this->_mantenimiento_output($output);
			}catch(Exception $e){
				show_error($e->getMessage().' --- '.$e->getTraceAsString());
			}
	}
          
		
	function aumental_cantidad_estudiante($post_array)
	{
		$id_nivel_actual = $post_array['id_nivel_actual'];
		$this->escuela->aumental_estudiante($id_nivel_actual);
                if(date('Y-m-d',strtotime($post_array['fechaingreso'])) >= date('Y-m-d'))
                {
                    $fecha_ar = explode("-", date('d-m-Y'));
                    if($fecha_ar[0] > 10)
                    {
                        $post_array['fecha_primer_pago'] = date("Y-m-d",strtotime("+1 months"));
                    }
                    else
                    {
                        $post_array['fecha_primer_pago'] = date("Y-m-d");
                    }
                }
                else
                {
                    $post_array['fecha_primer_pago'] = date('Y-m-d');
                    $post_array['condicion'] = 'Viejo';
                }
		return $post_array;
	}
	
	function estudiante_after_insert($post_array,$primary_key)
	{
		if($post_array['condicion'] === 'Nuevo')
		{
        	$this->escuela->set_ingreso($primary_key,'Pago Inscripción', $post_array['monto_inscripcion']);
		}
                $this->general_cuenta_pendiente();
	}
    
    function general_cuenta_pendiente()
    {
		$list_estudiante = $this->escuela->get_estudiantes();
		foreach($list_estudiante as $estudiante)
		{
			if(!$this->escuela->have_cuenta_por_cobra($estudiante['idestudiante']))
			{
				$this->escuela->set_cuenta_por_cobra(
					$estudiante['idestudiante'],
                                        'Pago Mensual',
					$estudiante['pago_mensual'],					
					'pendiente',
					0,
					$estudiante['pago_mensual'],
					date("Y-m-d")
				);
				$this->escuela->set_llamado($estudiante['idestudiante'], 'No');
				$this->escuela->set_have_cuenta($estudiante['idestudiante'], 'Si');
			}
			$this->_general_recargo($estudiante['idestudiante']);
			//if($this->escuela->cantidad_cuentapendiente($estudiante['idestudiante']) > 3)
			//{
				//$this->escuela->set_estudiante_inactivo($estudiante['idestudiante']);
			//}
		}
			
	}
	
	function _general_recargo($id_estudiante)
	{
		$resultado = $this->escuela->cuenta_pendiente_mes_pasado($id_estudiante);
		foreach($resultado as $row)
		{
			$this->escuela->set_recargo($row->idcuenta, '100', $row->monto + 100);
		}
	}
	
	function _get_nombre_profesor($value,$row)
	{
            $nivel = $this->escuela->get_nivel($row->id_nivel_actual);
            if(!empty($nivel))
                   $result = $this->escuela->get_nombre_profesor($nivel['id_docente']); 
            if(isset($result['nombre']))
                   return $result['nombre'];

            return '';
	}
        
        function _set_nombre_url($value,$row)
        {
            return '<a href='.site_url('perfil/'.$row->idestudiante).'>'.$value.'</a>';
        }
     
        function docente()
	{
            
                if (!$this->ion_auth->in_group('admin'))
		{
			$this->session->set_flashdata('message', 'Tiene que ser admin para ver esta pagina');
			redirect('/');
		}
		$crud = new grocery_CRUD();
		
		$crud->set_table('docente');
		$crud->set_subject('Empleado');
                $crud->set_theme('datatables');
		$crud->set_table_title('Empleados');
		$crud->set_relation_n_n('idioma', 'idioma_docente', 'idiomas', 'iddocente', 'ididioma', 'nombre');
		$crud->set_relation_n_n('dias', 'dia_docente', 'dias_laborables', 'iddocente', 'iddia', 'nombre');
		$crud->set_relation('id_familia', 'familia', 'apellidos');
		$crud->columns( 'nombre','id_familia', 'direccion','cedula', 'telefono' ,'celular', 'estadocivil', 'gradoacademico', 'sexo', 'fechanacimiento', 'edad', 'enfermedades', 'estatus', 'cargo','idioma','dias', 'fechaingreso', 'observaciones', 'monto_a_cobrar','nombre_pariente', 'telefono_pariente');
		
		$crud->fields('nombre', 'id_familia','idioma','dias', 'direccion','cedula', 'telefono' ,'celular', 'estadocivil', 'gradoacademico', 'sexo', 'fechanacimiento', 'edad', 'enfermedades', 'estatus', 'cargo', 'fechaingreso', 'observaciones', 'monto_a_cobrar','nombre_pariente', 'telefono_pariente');
		$crud->display_as('gradoacademico', 'Grado academico')
				 ->display_as('fechanacimiento', 'Fecha Nacimiento')
				 ->display_as('fechaingreso', 'Fecha Ingreso')
				 ->display_as('dias', 'Dias Laborables')
				 ->display_as('id_familia', 'Familia')
				  ->display_as('estadocivil', 'Estado Civil')
				   ->display_as('gradoacademico', 'Grado Académico')
				   ->display_as('direccion','Dirección')
				   ->display_as('telefono', 'Teléfono Residencial')
				   ->display_as('nombre_pariente', 'Nombre Pariente')
				   ->display_as('telefono_pariente', 'Teléfono Pariente')
				   ->display_as('monto_a_cobrar','Monto a Cobrar')
				   ->display_as('cedula', 'Cédula');
				
				 
		$crud->required_fields('nombre', 'id_familia', 'telefono', 'monto_a_cobrar');
		$output = $crud->render();
		
		$this->_mantenimiento_output($output);
	}
	
	function inscripcion()
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table('inscripcion');
                
		$crud->set_relation('idestudiante', 'estudiante', 'nombre');
		$crud->set_relation('iddocente', 'docente', 'nombre');
		$crud->set_relation('idcurso', 'curso', 'nombre');
		$output = $crud->render();
		
		$this->_mantenimiento_output($output);
	}
	
	function curso()
	{
		$crud = new grocery_CRUD();
		
		$crud->set_table('curso');
		$crud->set_subject('Curso');
                $crud->set_theme('datatables');
		$crud->set_relation('iddocente','docente','nombre');
		$crud->set_relation('idnivel','nivel','nombre');
		$crud->set_relation('ididioma','idiomas','nombre');				 
		$crud->display_as('iddocente', 'Nombre Docente')
			->display_as('idnivel', 'Nivel')
			->display_as('ididioma', 'Idioma')
			->display_as('montoinscripcion', 'Monto inscripcion')
			->display_as('montomensual', 'Monto Mensual')
			->display_as('cantidaddeestudiante', 'Cantidad Estudiante')
			->display_as('estudiantesinscriptos', 'Estudiantes Inscriptos')
			->display_as('horarioentrada', 'Horario Entrada')
			->display_as('horasalida', 'Horario Salida')
			->display_as('fechainicio', 'Fecha Inicio');
		$output = $crud->render();
		
		$this->_mantenimiento_output($output);
	}
	
	function idioma()
	{
       
		try{
			$crud = new grocery_CRUD();

			$crud->set_table('idiomas');
			$crud->set_subject('Idioma');
                        $crud->set_theme('datatables');
			$crud->set_table_title('Idiomas');
			$crud->required_fields('nombre');
			$crud->columns('nombre');
			$output = $crud->render();
			
			$this->_mantenimiento_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}

?>