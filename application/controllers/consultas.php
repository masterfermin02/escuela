<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Consultas extends CI_Controller {
	
    function __construct()
    {
            parent::__construct();
            $this->load->helper('flexigrid');
            $this->load->library('flexigrid');
            $this->load->library('ion_auth');

            $this->load->database();
            $this->load->helper('url');
            $this->load->model('escuela');
            $this->load->library('grocery_CRUD');

            $this->load->helper('url');

    }

    function _proceso_output($output = null)
    {
        $this->load->view('header.php',$output);
        $this->load->view('menu');
        $this->load->view('proceso.php',$output);	
        $this->load->view('footer.php');		
    }
    
    
    function ingreso()
    {
        //ver lib
		
		/*
		 * 0 - display name
		 * 1 - width
		 * 2 - sortable
		 * 3 - align
		 * 4 - searchable (2 -> yes and default, 1 -> yes, 0 -> no.)
		 */
		$colModel['Recibo'] = array('No. Recibo',40,TRUE,'center',2);
                $colModel['Nombre'] = array('Nombre',180,TRUE,'center',2);
                $colModel['Apellido'] = array('Apellido',180,TRUE,'center',2);
		$colModel['Concepto'] = array('Concepto',180,TRUE,'center',2);
                $colModel['Mes'] = array('Mes Pago',90,TRUE,'left',0);
		$colModel['Fecha'] = array('Fecha',90,TRUE,'left',0);
		$colModel['Monto'] = array('Monto',40,TRUE,'left',0);
		
		/*
		 * Aditional Parameters
		 */
		$gridParams = array(
		'width' => 900,
		'height' => 400,
		'rp' => 15,
		'rpOptions' => '[10,15,20,25,40,100,1000,2000,100000]',
		'pagestat' => 'Displaying: {from} to {to} of {total} items.',
		'blockOpacity' => 0.5,
		'title' => 'Ingresos',
		'usepager'=> true,
		'useRp' => true,
		'showTableToggleBtn' => true,
               
		);
		
		/*
		 * 0 - display name
		 * 1 - bclass
		 * 2 - onpress
		 */
		//$buttons[] = array('Delete','delete','test');
		$buttons[] = array('separator');
		//$buttons[] = array('Select All','add','test');
		//$buttons[] = array('DeSelect All','delete','test');
                $buttons[] = array('Print','add','print');
                $buttons[] = array('separator');
                $buttons[] = array('Export','add','exportar');

		
		//Build js
		//View helpers/flexigrid_helper.php for more information about the params on this function
		$grid_js = build_grid_js('flex1',site_url("consultas/search"),$colModel,'Recibo','desc',$gridParams,$buttons);
		
		$data['js_grid'] = $grid_js;
		$data['version'] = "0.36";
		$data['download_file'] = "Flexigrid_CI_v0.36.rar";
		//echo '<textarea>'.$grid_js.'</textarea>';
                $output = (object)array('output' => '' , 'js_files' => array() , 'css_files' => array());
		$this->load->view('header.php',$output);
                $this->load->view('menu');
		$this->load->view('consultas',$data);
    }
    
    function search()
    {
       $valid_fields = array('Recibo','Concepto','Fecha');
		
		$this->flexigrid->validate_post('Recibo','asc',$valid_fields);
		
		$records = $this->escuela->get_ingreso();
		
		$this->output->set_header($this->config->item('json_header'));
		
		/*
		 * Json build WITH json_encode. If you do not have this function please read
		 * http://flexigrid.eyeviewdesign.com/index.php/flexigrid/example#s3 to know how to use the alternative
		 */
		$record_items = array();

                $meses = array (
                    "January" =>"Enero",
                    "February" =>"Febrero",
                    "March" =>"Marzo",
                    "April" =>"Abril",
                    "May" =>"Mayo",
                    "June" =>"Junip",
                    "July" =>"Julio",
                    "August" =>"Agosto",
                    "September" =>"Septiembre",
                    "October" =>"Octubre",
                    "November" =>"Noviembre",
                    "December"=>"Diciembre",
                    "" => ""
                );                ;
        $monto_total = 0;
		foreach ($records['records']->result() as $row)
		{
			$record_items[] = array($row->Ingreso,
			$row->Recibo,
                        $row->Nombre,
                        $row->Apellido,
			$row->Concepto,
                        $meses[$row->Mes],
                        $row->Fecha,                       
			$row->Monto
            
			);
           $monto_total = $monto_total + $row->Monto;
		}
		//Print please
        $this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items,$monto_total));
        
    }
    
    function nivel_cursando()
    {
        $valid_fields = array('id_nivel','libro','unidades');
		
        $this->flexigrid->validate_post('id_nivel','asc',$valid_fields);

        $records = $this->escuela->get_nivel_list();

        $this->output->set_header($this->config->item('json_header'));

        /*
         * Json build WITH json_encode. If you do not have this function please read
         * http://flexigrid.eyeviewdesign.com/index.php/flexigrid/example#s3 to know how to use the alternative
         */
	$record_items = array();
        $monto_total = 0;
	foreach ($records['records']->result() as $row)
	{
		$record_items[] = array($row->id_nivel,
                    $row->id_nivel,
                    $row->libro,
                    $row->unidades,
                    $row->unidades_actuales,
                    $row->docente,
                    $row->horario
			);
       }
		//Print please
        $this->output->set_output($this->flexigrid->json_build($records['record_count'],$record_items,$monto_total));
    }
       
    function send_email()
    {
        $this->load->library('email');
        
        //$this->email->initialize($config);
        $this->email->from('your@example.com', 'Your Name');
        $this->email->to('masterfermin02@hotmail.com'); 
        
        $this->email->subject('Email Test');
        $this->email->message('Testing the email class.');	
        
        $this->email->send();
        
        echo $this->email->print_debugger();
    }
}
?>