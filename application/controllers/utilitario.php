<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utilitario extends CI_Controller {
	
	function __construct()
	{
            parent::__construct();
        }
        
        function backup()
        {
            $this->load->dbutil();
            $backup = $this->dbutil->backup();
            $this->load->helper('download');
            force_download('backup.gz',$backup);
			redirect('auth/login', 'refresh');
            
        }
}

/**
 * end file: Utilitario.php
 * location: application/controllers/Utilitario.php
 */