<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pago extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		$this->load->database();
		$this->load->helper('url');
		
		$this->load->library('grocery_CRUD');	
		$this->load->model('escuela');
		$this->load->library('ion_auth');
                $this->load->helper('reporte_helper');
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
	}
	
	function _pago_output($output = null)
	{
		$this->load->view('header.php',$output);
                $this->load->view('menu');
                $this->load->view('proceso.php',$output);	
		$this->load->view('footer.php');		
	}
	
	
	function index()
	{
		try{
			$crud = new grocery_CRUD();

			$crud->set_table('estudiante');
                        $crud->set_table_title('Cuentas Pendientes');
			$crud->where('estatus', 'activo');
			//$crud->where('Cantidad_Meses', '0');
		   // $crud->where('have_cuenta', 'Si');
			$crud->set_theme('datatables');
			$crud->set_subject('estudiante');
			//$crud->where("estatus",$filter);
			$crud->set_relation('idfamilia','familia','apellidos');
			$crud->set_relation('id_nivel_actual','nivel_cursando','libro');
			//$crud->set_relation('idestudiante','cuenta_pendiente','count(*) ');
			$crud->display_as('idfamilia', 'Apellidos Familia')
				->display_as('telefono', 'Teléfono')
				->display_as('llamado', 'Contactado')
				->display_as('estatus', 'Estado')
				 ->display_as('id_nivel_actual', 'Nivel Actual');
			$crud->columns('nombre','idfamilia','id_nivel_actual','telefono','Cantidad_Meses','llamado','estatus');
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
						
			$crud->add_action('Pagar Cuenta', base_url('imagenes/add.png'), 'pago/action_cuentas_por_pago','ui-icon-plus');
			$crud->add_action('Llamar', base_url('imagenes/add.png'), 'pago/llamar','ui-icon-plus');
			$crud->callback_column('Cantidad_Meses',array($this,'callback_column_cantidad_meses'));
			$output = $crud->render();
			
			$this->_pago_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function cuentas_pagadas()
	{
		try{
			
			$crud = new grocery_CRUD();

			$crud->set_table('cuenta_pendiente');
			$crud->set_table_title('Cuentas Pagadas');
			$crud->set_theme('datatables');
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->where('estado', 'pagado');
			 $crud->order_by('idcuenta','desc');
			 $crud->columns('idcuenta','idestudiante', 'concepto', 'monto', 'recargos', 'total', 'estado', 'fecha', 'mes');
			 $crud->add_action('Devolver Pago', base_url('imagenes/delete.png'), 'pago/devolver_pago','ui-icon-delete');
			 $crud->callback_column('mes',array($this,'callback_column_fecha'));
			$output = $crud->render();
			
			$this->_pago_output($output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function devolver_pago($cuenta)
	{
		$this->escuela->devolver_pago($cuenta);
		$this->delete_ingreso($cuenta);
		redirect('pago/cuentas_pagadas');
	}
	
	function callback_column_fecha($value,$row)
	{
		$mes = array("Enero","Febrero","Marzo", "Abril", "Mayo",
		"Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
		$pos_mes = substr($value, 5, 2) - 1;
		if($pos_mes < 0)
			$pos_mes = 0;
		return $mes[$pos_mes];
	}
	
	function callback_column_cantidad_meses($value,$row)
	{
		$meses = $this->escuela->cantidad_cuentapendiente($row->idestudiante);
		if($meses >= 1)
		{
			$this->escuela->set_have_cuenta($row->idestudiante,'Si');
		}elseif($meses == 0)
		{
			$this->escuela->set_have_cuenta($row->idestudiante,'No');
			//$row->estatus = 'inactive';
		}
		
		return $meses;
	}
	
	function action_cuentas_por_pago($id)
	{
		$data['cuentas'] = $this->escuela->get_cuenta_por_cobrar($id);
                $output = (object)array('output' => '' , 'js_files' => array() , 'css_files' => array());
		$this->load->view('header.php',$output);
                $this->load->view('pago.php',$data);
	}
	
	function llamar($id)
	{
		$this->escuela->set_llamado($id, 'Si');
		redirect('pago');
	}
	
	function pagar($id_estudiante)
	{
		$cuentas = $this->input->post();
                $data = array();
                $monto_total = 0;
                $mes = array("Enero","Febrero","Marzo", "Abril", "Mayo",
		"Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
                $secuencia = $this->escuela->agregar_recibo($id_estudiante);
                $data['secuencia'] = $secuencia;
		foreach($cuentas as $idcuenta)
		{
			$this->escuela->cuenta_pagar($idcuenta);
			$result = $this->escuela->get_cuenta_by_id_cuenta($idcuenta);
                        //Pasamos los datos que se van a mostrar en detalles de rebico para imprimir
                        $fecha = $result['fecha'];
						$pos_mes = substr($fecha, 5, 2) - 1;
						if($pos_mes < 0)
							$pos_mes = 0;
                        $data['rows'][] = array(
                        'concepto' => $result['concepto'],
                        'fecha' => date("d-m-Y", strtotime($result['fecha'])),
                        'mes' => $mes[$pos_mes],
                        'recargo' => $result['recargos'],
                        'monto' => $result['monto'],
                        'total' => $result['total']
                        );
                        $monto_total = $monto_total + $result['total'];
			$this->escuela->set_ingreso($result['idestudiante'],$result['concepto'], $result['total'], $secuencia,$idcuenta);
        }
                
                $data['monto_total'] = $monto_total;
                if($this->escuela->cantidad_cuentapendiente($id_estudiante) == 0)
                {
                    $this->escuela->set_have_cuenta($id_estudiante,'No');
                }
                $estudiante = $this->escuela->get_estudiante_by_id($id_estudiante);
                $nivel = $this->escuela->get_nivel($estudiante['id_nivel_actual']);
                               
                //Datos de la familia y el estudiante
                
                $data['familia'] = $this->escuela->get_apellido_familia($estudiante['idfamilia']);
                $data['direccion'] = $estudiante['direccion'];
                $data['telefono'] = $estudiante['telefono'];
                $nivel['libro'] = isset($nivel['libro']) ? $nivel['libro'] : 'Libro';
                $nivel['unidades_actuales'] = isset($nivel['unidades_actuales']) ? $nivel['unidades_actuales'] : 'Unidades Actuales'; 
               $data['results'] = array($estudiante['idestudiante'], $estudiante['nombre'], $nivel['libro'], $nivel['unidades_actuales']);                
               $this->load->view('reporte/recibo_html', $data);
		
			
	}
    
    function pagar_nomina()
    {
        $empleados = $this->escuela->get_empleados();
		$nomina = array();
		foreach($empleados as $empleado)
		{
			$turnos = $this->escuela->get_turnos($empleado->iddocente);
			if($turnos == 0)
			{
				$turnos = 1;
			}
			
		}
    }
	
	function print_recibo_html()
	{
		$data['secuencia'] = 500;
		$data['monto_total'] = 500;
		$data['rows'][] = array(
                        'concepto' => 'Pago mes',
                        'fecha' => '2015/12/17',
                        'mes' => 'Dic',
                        'recargo' => 0,
                        'monto' => 500,
                        'total' => 500
                        );
		//if($this->escuela->cantidad_cuentapendiente($id_estudiante) == 0)
		//{
			//$this->escuela->set_have_cuenta($id_estudiante,'No');
		//}
		//$estudiante = $this->escuela->get_estudiante_by_id(33);
		//$nivel = $this->escuela->get_nivel($estudiante['id_nivel_actual']);
					   
		//Datos de la familia y el estudiante
		
		$data['familia'] = 'test';
		$data['direccion'] = 'Test dire';
		$data['telefono'] = 'test';
		
	   $data['results'] = array(1, 'tets banme', 'test',
								 'test' 
							);                
	   $this->load->view('reporte/recibo_html', $data);
	}


}

/*
?>*/