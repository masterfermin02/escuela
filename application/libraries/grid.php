<?php


class Grid 
{
    /* private instance of variable */
    private $table = null;
    private $table_columns = null;
    private $fields = null;
   //private $table_contents = null;
    
     public function __construct()
    {
        
    }
    
    public function set_table($table)
    {
        $this->table = $table;
        $this->_set_fields();
    }
    
    public function set_table_columns($table_columns)
    {
        $this->table_columns = $table_columns;
    }
    
    private function _set_fields()
    {
        $CI =& get_instance();
        $fields = $CI->db->field_data($this->table);

        foreach ($fields as $field)
        {
           $this->fields[] = $field;
        }
    }
    
    public function display_as($name, $display)
    {
        
    }
    
    public function render()
    {
        
        $content = '<table cellpadding="0" cellspacing="0" id="data">
        	<thead>
            	<tr> ';
        foreach($this->fields as $item)
                     $content =  $content.'<th width="22%"><span title='.$item->name.' >'.$item->name.'</span></th>';
                    
               $content = $content.'</tr>
            </thead>
            <tbody>
            	
            </tbody>
        </table>';
        return $content;
    }
    
    public function get_table()
    {
        return $this->table;
    }
    
}



?>