<?php
    require('PDF_MySQL_Table.php');

    class PDF extends PDF_MySQL_Table
    {
        function Header()
        {
            //Title
            $this->SetFont('arial','B',12);
            $this->Cell(0,6,'EBG LANGUAGE CENTER ( L.C)',0,1,'C');
            $this->SetFont('arial','B',10);
            $text = utf8_decode('SHARING KNOWLEDGE WITH PRIDE');
            $this->cell(0,6,$text,0,1,'C');
            $text = utf8_decode('Carretera Luperón # 43 km 2 ½, Gurabo, Santiago R. D');
            $this->Cell(0,3,$text,0,1,'C');
            $text = utf8_decode('Correo electrónico: ebglanguagecenter@gmail.com, Tel: 809-581-5555');
            $this->Cell(0,3,$text,0,1,'C');
            $hora = getdate(time());
            $fecha = 'Fecha : '.date('d-m-Y').' '.$hora["hours"].':'.$hora["minutes"].':'.$hora["seconds"];
            $this->Cell(0,3,$fecha,0,1,'R');
            $this->SetFont('arial','B',12);
            $this->ln(4);
            $this->cell(0,6,'RECIBO DE PAGO Y MESUALIDAD',0,1,'C');
            $this->Ln(2);
            //Ensure table header is output
            parent::Header();
        }
    }
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
?>