<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Centro Educativo Baldera Gil</title>
<link href="<?php echo base_url(); ?>public/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url(); ?>public/css/flexigrid.css" rel="stylesheet" media="screen" type="text/css" />
<link href="<?php echo base_url(); ?>public/css/flexigrid.css" rel="stylesheet" media="print" type="text/css" />
<link rel="stylesheet" type="text/css" href=<?php echo base_url("assets/jqueryui/css/smoothness/jquery-ui-1.8.16.custom.css"); ?> />
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/flexigrid.js"></script>
<script type="text/javascript" src=<?php echo base_url("assets/jqueryui/js/jquery-ui-1.8.16.custom.min.js"); ?>></script>
<script type="text/javascript" src=<?php echo base_url("assets/grocery_crud/themes/flexigrid/js/jquery.printElement.min.js"); ?> ></script>
<style>
    .grid-footer {
position: relative;
top: -28px;
float : right;
width : 200px;
}

.grid-footer .pGroup {
   padding-top: 5px;
}
</style>
</head>
<body>
<h1>Consultas</h1>
    
<div class="egBox">
    <fieldset style="width: 875px;">
	<legend>Buscar por Rango Fecha</legend>
	<form id="nform">
		Search del : <input type="text" readonly="readonly" name="del" id="del" size="15" value="" autocomplete="off"/> al : 
        <input type="text" readonly="readonly" name="al" id="al" size="15" class="datepicker"value="" autocomplete="off"/>
        <input type="submit" value="Search" />
        <input type="button" value="Clean" onclick="clean()" />
	</form>
</fieldset>
<table id="flex1" style="display:none"></table>
    <div class="grid-footer flexigrid">
    <div class="btnseparator"></div>
    <div class="pGroup">
    <span class="pPageStat" id="monto_total">
    Total : <?php if(isset($total))
		echo $total;
	?>
    </span>
    </div>
</div>    
<script type="text/javascript">
    var print_window;
var dates = $( "#del, #al" ).datepicker({
			yearRange: "-50",
			defaultDate: "+1w",
			changeMonth: true,
			changeYear: true,
			onSelect: function( selectedDate ) {
				var option = this.id == "del" ? "minDate" : "maxDate",
					instance = $( this ).data( "datepicker" ),
					date = $.datepicker.parseDate(
						instance.settings.dateFormat ||
						$.datepicker._defaults.dateFormat,
						selectedDate, instance.settings );
				dates.not( this ).datepicker( "option", option, date );
			}
	});
<?php echo $js_grid; ?>
    
function test(com,grid)
{
    if (com=='Select All')
    {
		$('.bDiv tbody tr',grid).addClass('trSelected');
    }
    
    if (com=='DeSelect All')
    {
		$('.bDiv tbody tr',grid).removeClass('trSelected');
    }
    
    if (com=='Delete')
        {
           if($('.trSelected',grid).length>0){
			   if(confirm('Delete ' + $('.trSelected',grid).length + ' items?')){
		            var items = $('.trSelected',grid);
		            var itemlist ='';
		        	for(var i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
					   type: "POST",
					   url: "<?php echo site_url("/ajax/deletec");?>",
					   data: "items="+itemlist,
					   success: function(data){
					   	$('#flex1').flexReload();
					  	alert(data);
					   }
					});
				}
			} else {
				return false;
			} 
        }          
}
    function print(com,grid)
    {
        if (com=='Print')
        {
            var opciones="toolbar=no, location=<?php echo site_url("eporte/export_ingreso");?>
                , directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";
            window.open("<?php echo site_url("reporte/ingreso");?>","",opciones);           
            
        }
    }
	
	function exportar()
	{
                var del = $('#del').val();
                var al = $('#al').val();
		window.location = "<?php echo site_url();?>"+"/reporte/export_ingreso?del="+del+"&al="+al;
		
	}
    
    function clean()
    {
        $('#del').val('');
        $('#al').val('');
        var dt = $('#nform').serializeArray();
    	$("#flex1").flexOptions({params: dt});
        $('#flex1').flexOptions({newp: 1}).flexReload();
        
    }
 
    $('#nform').submit(function (){
    var dt = $('#nform').serializeArray();
    $("#flex1").flexOptions({params: dt});
    $('#flex1').flexOptions({newp: 1}).flexReload();
    return false;
       
});

    function get_monto_total()
    {
        var campo5 = 0;
       $("#flex1 tbody tr").each(function (index) {
                 
                 $(this).children("td").each(function (index2) {
                     switch (index2) {
                         
                        case 4:
                             campo5 = campo5 + parseFloat($(this).text());
                            break;
                    }
                
                })
                                    
            });
                             
            add_total(campo5);
    }

  $("#flex1").ajaxSuccess(function(event,xhr,options){
            var jsonObj = xhr.responseText;
            var obj = $.parseJSON(jsonObj);
            add_total(obj.monto);
        });  
    function add_total(str)
    {
         $('#monto_total').empty();
        $('#monto_total').append('Total : $ ' + addCommas(str));
    }
     
    function clean_total()
    {
        $('#monto_total').empty();
        $('#monto_total').append('Total : ');
    }
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
</script>
</body>
</html>