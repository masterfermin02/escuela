<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<link type="text/css" href=<?php echo base_url("assets/css/style.css") ?> rel="stylesheet" media="screen" />
<link type="text/css" href=<?php echo base_url("assets/css/bootstrap.min.css") ?> rel="stylesheet" media="screen" />

<link href="<?php echo base_url(); ?>public/css/flexigrid.css" rel="stylesheet" media="screen" type="text/css" />


<style type="text/css" >

    .grid-footer {
    position: relative;
    top: -28px;
    float : right;
    width : 100px;
    }

    .grid-footer .pGroup {
       padding-top: 5px;
    }
</style>
</head>
<body >
	