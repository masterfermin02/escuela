<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href=<?php echo base_url("js/css/core.css") ?> rel="stylesheet" media="screen" type="text/css" />
    <link href=<?php echo base_url("js/css/core.css") ?> rel="stylesheet" media="print" type="text/css" />
	<link type="text/css" href=<?php echo base_url("assets/css/table.css") ?> rel="stylesheet" media="screen" />
    <link type="text/css" href=<?php echo base_url("assets/css/table.css") ?> rel="stylesheet" media="print" />
    <link type="text/css" href=<?php echo base_url("assets/css/style.css") ?> rel="stylesheet" media="screen" />
    <link type="text/css" href=<?php echo base_url("assets/css/style.css") ?> rel="stylesheet" media="print" />
<title>Reporte Nomina</title>
</head>
<body >

<div id="content">
<table id="box-table-a" style="margin-left: auto;  margin-right: auto;">
<thead >
<tr>
	<th >Nombre</th>
  <th>Familia</th>
    <th>Turno</th>
    <th>Monto</th>
    <th>Descuento AFP</th>
    <th>Descuento ARS</th>
  <th >Total</th>
</tr>
</thead>
<?php 	$i = 1; 
		$total = 0;
?>
<tbody style="background-color:#0099CC; color: #800080; ">
<?php foreach ($nominas as $items): ?>

	<tr>
		<td>
		<?php echo $items['nombre']; ?>

	  </td>
	   <td>
		<?php echo $items['familia']; ?>

	  </td>
	  <td>
		<?php echo $items['turnos']; ?>

	  </td>
	  <td>
		<?php echo $items['monto']; ?>

	  </td>
	  <td ><?php $descuentoars = $items['total'] * 0.0287;
            echo $descuentoars; ?></td>
        <td ><?php $descuentoafp =  $items['total'] * 0.0304;
echo $descuentoafp;?></td>
        <td ><?php $total_item = $items['total'] - $descuentoars -  $descuentoafp; 
            echo $total_item;
            ?></td>
	  <?php $total = $total + $total_item;?>
	</tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
	<td></td>
	<td></td>
	<td></td>
  <td style="text-align:right"><strong>Total</strong></td>
  <td style="text-align:right">$<?php echo $total?></td>
</tr>
<tbody>
</table>
    <a href=<?php echo site_url('reporte/export_nomina') ?> class="button" rel="content">EXPORT</a> <a href="#" class="print" rel="content">print</a>
</div>
    <script src="http://localhost/escuela/js/jquery-1.6.2.min.js"></script>
<script src="http://localhost/escuela/js/jquery.PrintArea.js_4.js"></script>
<script src="http://localhost/escuela/js/core.js"></script>
    <script type="text/javascript">
        $(function(){
            $('.print').click(function(){
                var content = $(this).attr("rel");
                $('#' + content).printArea();
            });
        });
    </script>
</body>
</html>