
<div class="page-header">
  <h1>Administrador de correo <small>EBG</small></h1>
</div>
<div class="panel panel-primary">
  <div class="panel-heading">Creando correo</div>
  <div class="panel-body">

  	<div class="form-group">
  		<label for="titulo" class="col-sm-2 control-label">Titulo:</label>
	    <input type="text" class="form-control" name="titulo" />
  	</div>
  	<div class="form-group">
  		<label for="para" class="col-sm-2 control-label">Para:</label>
	    <input type="text" class="form-control" name="para" />
  	</div>
  	<div class="form-group">
	  	<label for="mesanje" class="col-sm-2 control-label">Mensaje:</label>
	    <textarea class="form-control" rows="3" >
	    	</textarea>
    </div>
    <div class="form-group">
    	<span class="pull-right btn btn-success" >Enviar</span>
    </div>
  </div>
</div>

