<!-- 
 * parallax_login.html
 * @Author original @msurguy (tw) -> http://bootsnipp.com/snippets/featured/parallax-login-form
 * @Tested on FF && CH
 * @Reworked by @kaptenn_com (tw)
 * @package PARALLAX LOGIN.
-->
        <script src="http://mymaplist.com/js/vendor/TweenLite.min.js"></script>
        <body>
            <div class="container">
                <div class="row vertical-offset-100">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="panel panel-default">
                            <div class="panel-heading">                                
                                <div class="row-fluid user-row">
                                    <img src="http://s11.postimg.org/7kzgji28v/logo_sm_2_mr_1.png" class="img-responsive" alt="Conxole Admin"/>
                                </div>
                            </div>
                            <div class="panel-body">
                                <?php 
								$attributes = array('class' => 'form-control');
								echo form_open("auth/login");?>
                                    <fieldset>
                                        <label class="panel-login">
                                            <div class="login_result"><?php echo $message;?></div>
                                        </label>
										<table>
										<tr>
										<td>
										 <?php echo "<label>Username:</label>";?>
										 </td>
										 <td>
										<?php 
										
										echo form_input($identity, $attributes);?>
                                        <!--<input class="form-control" placeholder="Username" id="username" type="text">-->
										</td>
										</tr>
										<td>
										<?php echo lang('login_password_label', 'password');?>
										</td>
										<td>
										<?php echo form_input($password);?>
										</td>
										</tr>
										<tr>
                                        <!--<input class="form-control" placeholder="Password" id="password" type="password">-->
										<td>
										<?php echo lang('login_remember_label', 'remember');?>
										</td>
										<td>
										<?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
										</td>
										</tr>
										</table>
                                        <br></br>
                                        <input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login ">
										<p><a href="forgot_password"><?php echo lang('login_forgot_password');?></a></p>
                                    </fieldset>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			

        </body>
            </div>