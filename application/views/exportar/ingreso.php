<?php
 header("Content-Type: application/vnd.ms-excel");
 header("Expires: 0");
 header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
 header("content-disposition: attachment;filename=ingresos".date('d-m-Y').".xls");
?>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Ingresos</title>

<style type="text/css">

<!–

.style1 {

font-family: Verdana, Arial, Helvetica, sans-serif;

font-weight: bold;

}

.style2 {font-family: Verdana, Arial, Helvetica, sans-serif}

–>

</style>

</head>

<body>
<table>
    <thead>
        <td>
            No. Recibo
        </td>
        <td>
            Nombre
        </td>
        <td>
            Apellido
        </td>
        <td>
            Concepto
        </td>
		<td>
            Mes Pagado
        </td>
        <td>
            Fecha de Pago
        </td>
        <td>
            Monto
        </td>
    </thead>
    <tbody>
    <?php 
        $monto_total = 0;
        foreach($ingresos as $item)
        {
            echo '<tr>';
            echo '<td>'.$item->Recibo.'</td>';
            echo '<td>'.$item->Nombre.'</td>';
            echo '<td>'.$item->Apellido.'</td>';
            echo '<td>'.$item->Concepto.'</td>';
            echo '<td>'.$item->Mes.'</td>';
            echo '<td>'.$item->Fecha.'</td>';
            echo '<td>'.$item->Monto.'</td>';
            echo '</tr>';
            $monto_total = $monto_total + $item->Monto;
        }
    ?>
        <tr colspan="3">
            <td></td>
            <td style="text-align: right;">Total : </td>
            <td> <?php echo $monto_total; ?> </td>
        </tr>
    </tbody>
    </table>
</body>
</html>