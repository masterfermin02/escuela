<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
<?php 
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<link type="text/css" href=<?php echo base_url("assets/css/style.css") ?> rel="stylesheet" media="screen" />
<script src=<?php echo base_url("js/jquery.maskedinput.js") ?> type="text/javascript"></script>
<script src=<?php echo base_url("js/jquery.autocomplete.js") ?> type="text/javascript"></script>
<link href="<?php echo base_url(); ?>public/css/flexigrid.css" rel="stylesheet" media="screen" type="text/css" />
<script type="text/javascript" >
 jQuery(function($){
        $("#field-telefono").mask("(999)999-9999");
    });
	jQuery(function($){
        $("#field-celular").mask("(999)-999-9999");
    });
	jQuery(function($){
        $("#field-cedula").mask("999-9999999-9");
    });
	jQuery(function($){
        $("#field-telefono_empresa").mask("(999)-999-9999");
    });
	jQuery(function($){
        $("#field-telefono_pariente").mask("(999)-999-9999");
    });

$(document).ready(function(){
	$("#field-fechanacimiento").change(function () {
			var fecha_actual = new Date();
			var fecha_nacimiento = $("#field-fechanacimiento").val();
			fecha_nacimiento = fecha_nacimiento.substr(6,4);
			var edad = fecha_actual.getFullYear() - fecha_nacimiento;
		  $("#field-edad").val(edad);
	});
});
	
	</script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/js/flexigrid.js"></script>

<style type="text/css" >

    .grid-footer {
    position: relative;
    top: -28px;
    float : right;
    width : 100px;
    }

    .grid-footer .pGroup {
       padding-top: 5px;
    }
</style>
</head>
<body >
	<div id="wrapper">