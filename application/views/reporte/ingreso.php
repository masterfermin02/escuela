<!DOCTYPE html>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href=<?php echo base_url("js/css/core.css") ?> rel="stylesheet" media="screen" type="text/css" />
    <link href=<?php echo base_url("js/css/core.css") ?> rel="stylesheet" media="print" type="text/css" />
	<link type="text/css" href=<?php echo base_url("assets/css/table.css") ?> rel="stylesheet" media="screen" />
    <link type="text/css" href=<?php echo base_url("assets/css/table.css") ?> rel="stylesheet" media="print" />
    <link type="text/css" href=<?php echo base_url("assets/css/style.css") ?> rel="stylesheet" media="screen" />
    <link type="text/css" href=<?php echo base_url("assets/css/style.css") ?> rel="stylesheet" media="print" />
<title>Reporte Ingresos</title>

<style type="text/css">

<!–

.style1 {

font-family: Verdana, Arial, Helvetica, sans-serif;

font-weight: bold;

}

.style2 {font-family: Verdana, Arial, Helvetica, sans-serif}

–>

</style>

</head>

<body>
    <div id="content">
<table id="box-table-a">
    <thead>
        <tr>
        <th>
            No. Recibo
        </th>
        <th>
            Nombre
        </th>
        <th>
            Apellido
        </th>
        <th>
            Fecha
        </th>
        <th>
            Monto
        </th>
        </tr>
    </thead>
    <tbody>
    <?php 
        $monto_total = 0;
        foreach($ingresos as $item)
        {
            echo '<tr>';
            echo '<td>'.$item->Recibo.'</td>';
            echo '<td>'.$item->Nombre.'</td>';
            echo '<td>'.$item->Apellido.'</td>';
            echo '<td>'.$item->Concepto.'</td>';
            echo '<td>'.$item->Fecha.'</td>';
            echo '<td>'.$item->Monto.'</td>';
            echo '</tr>';
            $monto_total = $monto_total + $item->Monto;
        }
    ?>
        <tr >
            <td></td>
            <td style="text-align: right;">Total : </td>
            <td> <?php echo $monto_total; ?> </td>
        </tr>
    </tbody>
    </table>
    </div>
<script src="http://localhost/escuela/js/jquery-1.6.2.min.js"></script>
<script src="http://localhost/escuela/js/jquery.PrintArea.js_4.js"></script>
<script src="http://localhost/escuela/js/core.js"></script>
    <script type="text/javascript">
        $(window).load(function(){ 
            var content = "content";
                $('#' + content).printArea();
        });
    </script>
</body>
</html>