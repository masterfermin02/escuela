<div style='text-align:center' >
<?php
    $hora = getdate(time());
	echo "<p>EBG LANGUAGE CENTER ( L.C) <br />";
    echo "SHARING KNOWLEDGE WITH PRIDE <br />";
    echo ("Carretera Luperon # 43 km 2 , Gurabo, Santiago R. D <br />");
    echo ("Correo electronico: ebglanguagecenter@gmail.com, Tel: 809-581-5555<br /></p>");
    echo ("Fecha : ".date('d-m-Y')." ".$hora["hours"].":".$hora["minutes"].":".$hora["seconds"]."<br />");
    echo ("=========RECIBO DE PAGO Y MESUALIDAD====================");
    echo "<p style='text-align:left'>Numero Recibo  : ".$secuencia."<br />";
    echo "Familia        : ".$familia."<br />";
    echo "Direccion      : ".$direccion."<br />";
    echo "Telefono       : ".$telefono."</p>";
	?>
	<table style="width:100%" >
		<thead>
			<tr>
			<th>Matricula</th><th>Nombre</th><th>Libro</th><th>Unidad Actual</th>
			</tr>
		</thead>
		<tbody>
			<tr style='text-align:center' >
				<td><?php echo $results[0]; ?></td>
				<td><?php echo $results[1]; ?></td>
				<td><?php echo $results[2]; ?></td>
				<td><?php echo $results[3]; ?></td>
			</tr>
		</tbody>
	</table>
	<table style="width:100%" >
		<thead>
			<tr>
			<th>Concepto</th><th>Fecha</th><th>Mes</th><th>Recargo</th><th>Monto</th><th>Total</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($rows as $row) : ?>
			<tr style='text-align:center' >
				<td ><?php echo $row['concepto']; ?></td>
				<td><?php echo $row['fecha']; ?></td>
				<td><?php echo $row['mes']; ?></td>
				<td><?php echo $row['recargo']; ?></td>
				<td><?php echo $row['monto']; ?></td>
				<td><?php echo $row['total']; ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<?php echo 'Total Recibido : $'.number_format($monto_total);  ?>
 </div>