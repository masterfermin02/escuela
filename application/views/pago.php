<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href=<?php echo base_url("assets/css/table.css")?> >
</head>
<body >

<?php echo form_open('pago/pagar/'.$this->uri->segment(3)); ?>

<table id="box-table-a" style="margin-left: auto;  margin-right: auto;">
<thead >
<tr>
	<th >Select Cuenta</th>
  <th>Concepto</th>
    <th>Estado</th>
  <th>Fecha</th>
  <th>Mes</th>
  <th >Monto</th>
  <th >Recargo</th>
  <th >Sub-total</th>
</tr>
</thead>
<?php 	$i = 1; 
		$total = 0;
		$mes = array("Enero","Febrero","Marzo", "Abril", "Mayo",
		"Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
?>
<tbody style="background-color:#0099CC; color: #800080; ">
<?php foreach ($cuentas as $items): ?>

	<tr>
		<td>
			<?php echo form_checkbox($i.'cuenta', $items->idcuenta, TRUE);?>
		</td>
        <td>
		<?php echo $items->concepto; ?>

	  </td>
	   <td>
		<?php echo $items->estado; ?>

	  </td>
	  <td>
		<?php echo date("d-m-Y", strtotime($items->fecha)); ?>

	  </td>
	  <td>
		<?php $fecha = $items->fecha;
                    $pos_mes = substr($fecha, 5, 2) - 1;
                    if($pos_mes < 0)
                        $pos_mes = 0;
			echo $mes[$pos_mes];
		?>

	  </td>
	  <td ><?php echo $items->monto; ?></td>
	  <td><?php echo $items->recargos; ?></td>
	  <td style="text-align:right">$<?php echo $items->total; ?></td>
	  <?php $total = $total + $items->total?>
	</tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
	<td></td>
	<td></td>
	<td></td>
   
  <td style="text-align:right"><strong>Total</strong></td>
    
  <td style="text-align:right">$<?php echo $total?></td>
    
    
</tr>
<tbody>
</table>

<p><?php echo form_submit('', 'Pagar'); ?></p>
<script type="text/javascript">
    function open_window()
    {
        window.open('http://localhost/escuela/index.php/');
    }
</script>
</body>
</html>