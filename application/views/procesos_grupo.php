<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href=<?php echo base_url("assets/css/table.css")?> >

    <script type="text/javascript">
        $('.genera_re-inscripcion').live('click', function(){
				$(this).prop( "disabled", true );
                  var id = $(this).attr('href');
                  $.ajax({
                      url: "<?php echo site_url('procesos_grupo/general_cuenta'); ?>/" + id,
                      success: function(){
                         alert('La Re-inscripcion se a generado correctamente');
                      }
                  });
                  $(this).prop( "disabled", false);       
                return false;
              });
        
    </script>
</head>
<body >
<?php echo $output; ?>
<div class="flexigrid" style="width: 100%;">
<div class="tDiv">
<div class="tDiv3">
			        	<a class="export-anchor" data-url="<?php echo site_url('reporte/export_group')."/".$grupo_id ?>"target="_blank">
				<div class="fbutton">
					<div>
						<span class="export">Export</span>
					</div>
				</div>
            </a>
			<div class="btnseparator"></div>
									
		</div>
	<div class="clear"></div>
	</div>
	<div class="bDiv">
<table >
<thead >
<tr>
	<th >Nombre</th>
  <th>Dirección</th>
  <th>Teléfono</th>
  <th>Celular</th>
  <th >Fecha Nacimiento</th>
  <th >Edad</th>
  <th >Monto Reinscripción</th>
</tr>
</thead>
<tbody >
<?php foreach ($estudiantes as $items): ?>

	<tr>
		<td>
			<?php echo  $items->nombre." ".$items->apellidos ?>
		</td>
	   <td>
		<?php echo $items->direccion ?>

	  </td>
        <td>
		<?php echo $items->telefono ?>

	  </td>
        <td>
		<?php echo $items->celular ?>

	  </td>
	  <td>
		<?php echo date("d-m-Y", strtotime($items->fechanacimiento)); ?>

	  </td>
	  <td>
		<?php echo $items->edad; ?>

	  </td>
	  <td ><?php echo $items->monto_reinscripcion; ?></td>
	  
	</tr>

<?php endforeach; ?>
<tbody>
</table>
</div> 
	</div>
	   
 
</body>
</html>