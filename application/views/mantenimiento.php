
<div>
<?php if(isset($list_curso)): ?>
    <table id="flex2" style="display:none"></table>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/flexigrid.js"></script>
    
    <script type="text/javascript">
        <?php echo $list_curso; ?>
        (function($){
            $(document).ready(function(){
        
                $('#flex2').click(function(){
                    $id = $('.trSelected','#flex2')[0].id.substr(3);
                    $('#field-id_nivel_actual').val($id);
               });

             });
            $("#flex1").ajaxSuccess(function(event,xhr,options){
                var jsonObj = xhr.responseText;
                var obj = $.parseJSON(jsonObj);
                add_total(obj.monto);
            });  
        }(jQuery));
     
       
function test(com,grid)
{
    if (com=='Select All')
    {
		$('.bDiv tbody tr',grid).addClass('trSelected');
    }
    
    if (com=='DeSelect All')
    {
		$('.bDiv tbody tr',grid).removeClass('trSelected');
    }
    
    if (com=='Delete')
        {
           if($('.trSelected',grid).length>0){
			   if(confirm('Delete ' + $('.trSelected',grid).length + ' items?')){
		            var items = $('.trSelected',grid);
		            var itemlist ='';
		        	for(i=0;i<items.length;i++){
						itemlist+= items[i].id.substr(3)+",";
					}
					$.ajax({
					   type: "POST",
					   url: "<?php echo site_url("/ajax/deletec");?>",
					   data: "items="+itemlist,
					   success: function(data){
					   	$('#flex1').flexReload();
					  	alert(data);
					   }
					});
				}
			} else {
				return false;
			} 
        }          
}
    function print(com,grid)
    {
        if (com=='Print')
        {
            var opciones="toolbar=no, location=<?php echo site_url("eporte/export_ingreso");?>
                , directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, width=508, height=365, top=85, left=140";
            window.open("<?php echo site_url("reporte/ingreso");?>","",opciones);           
            
        }
    }
	
	function exportar()
	{
		window.location = "<?php echo site_url("reporte/export_ingreso");?>";
		
	}
    
    function clean()
    {
        $('#del').val('');
        $('#al').val('');
        var dt = $('#nform').serializeArray();
    	$("#flex1").flexOptions({params: dt});
        $('#flex1').flexOptions({newp: 1}).flexReload();
        
    }
 
    $('#nform').submit(function (){
    var dt = $('#nform').serializeArray();
    $("#flex1").flexOptions({params: dt});
     $('#flex1').flexOptions({newp: 1}).flexReload();
    return false;
       
});

    function get_monto_total()
    {
        var campo5 = 0;
       $("#flex1 tbody tr").each(function (index) {
                 
                 $(this).children("td").each(function (index2) {
                     switch (index2) {
                         
                        case 4:
                             campo5 = campo5 + parseFloat($(this).text());
                            break;
                    }
                
                })
                                    
            });
                             
            add_total(campo5);
    }

  
    function add_total(str)
    {
         $('#monto_total').empty();
        $('#monto_total').append('Total : $ ' + addCommas(str));
    }
     
    function clean_total()
    {
        $('#monto_total').empty();
        $('#monto_total').append('Total : ');
    }
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }

    </script>
    <?php endif; ?>
</div>
<div>
    
    
		<?php echo $output; ?>
    </div>
