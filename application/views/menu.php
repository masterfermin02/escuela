

	<nav class="navbar " >
		<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <li  >
      	<a href="<?php echo site_url()?>" >Mantenimientos</a>
				<ul  >
					<li><a href='<?php echo site_url('mantenimiento/familia')?>'>Familias</a> </li>
					<li><a href='<?php echo site_url('mantenimiento/docente')?>'>Empleados</a></li>
					<li><a href='<?php echo site_url('mantenimiento/estudiante')?>'>Estudiantes</a>  </li>
					<li><a href='<?php echo site_url('mantenimiento/estudiante_inactivos')?>'>Estudiantes Inactivos</a>  </li>
					<li><a href='<?php echo site_url('mantenimiento/nivel_cursando')?>'>Nivel en Curso</a> </li>
					<li><a href='<?php echo site_url('mantenimiento/idioma')?>'>Idiomas</a> </li>
					<!--<li><a href='<?php// echo site_url('mantenimiento/padre')?>'>Padres</a> </li>-->					
				</ul>
			</li>
			<li  >
				<a href="<?php echo site_url()?>">Consultas</a>
                <ul >
                    <li><a href="<?php echo site_url('consultas/ingreso')?>">Ingresos</a></li>
                </ul>
                </li>
			<li><a href="<?php echo site_url()?>">Procesos</a>
				<ul  >
					<li><a href='<?php echo site_url('procesos/general_cuenta_pendiente'); ?>' >Cuenta Por Cobrar</a></li>
                    <li><a href='<?php echo site_url('procesos_grupo')?>'>Cambio de Grupo</a></li>
                    <li><a href='<?php echo site_url('procesos/nomina')?>'>Nomina</a></li>
                    <li><a href='<?php echo site_url('pago/cuentas_pagadas')?>'>Cuentas Pagadas</a></li>
                    <li><a href='<?php echo site_url('email')?>' >Email</a></li>
				</ul>
			<li><a href="#">Utilitarios</a>  
			<ul   >
				<li><a href='<?php echo site_url('utilitario/backup')?>'>Backup</a></li>
                <li><a href='<?php echo site_url('auth')?>'>Usuarios</a></li>
			</ul>
			</li>
			<li>
				<?php
		
		$user = $this->ion_auth->user()->row();
		echo '<a href='.site_url("auth/logout").'>'.$user->username.' Logout</a>';
	?>
			</li>
      </ul>
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>